<?php
// include 'app/db.php';
// include 'app/func.php';
	
if (isset($_GET['pageno']) && is_numeric($_GET['pageno'])) {
    $pageno = $_GET['pageno'];
} else {
    $pageno = 1;
}
$no_of_records_per_page = 10;
$offset = ($pageno - 1) * $no_of_records_per_page;
$total_pages_sql = getCountAirdrops();
$total_pages = ceil($total_pages_sql / $no_of_records_per_page);
$page = '';
$prev = '';
$next = '';

if ($pageno != 1) {
	$prev = $pageno - 1;
	$prev = "<li class=\"page-item\"><a href=\"?pageno=$prev".searchForPagination().sortForPagination()."\" class=\"page-link text-dark\">Назад</a></li>";
}
if ($pageno >= 1 && $pageno < $total_pages) {
	$next = $pageno + 1;
	$next = "<li class=\"page-item\"><a href=\"?pageno=$next".searchForPagination().sortForPagination()."\" class=\"page-link text-dark\">Вперед</a></li>";
}

for ($i=1; $i <= $total_pages; $i++) {

	if ($pageno == $i) {
		$class = ' active';
	} else $class = '';

	$page .= " <li class=\"$class page-item\"><a href=\"?pageno=$i".searchForPagination().sortForPagination()."\" class=\"page-link text-dark\">$i</a></li>";
}

$render = $prev.$page.$next;


