<?php
$routes = [
	'/' => 'index',
	'/test' => 'cs',
    'about' => 'about',
 	'politics' => 'politics',
 	'disclaimer' => 'disclaimer',
 	'rules' => 'rules',
 	'support' => 'support',
 	'airdrop/([a-zA-Z0-9_\-]+)' => 'airdrop'
];

// if (!empty($_SERVER['REQUEST_URI'])) {
//     $uri = trim($_SERVER['REQUEST_URI'], '/');
// }

// foreach ($routes as $uriPattern => $path) {
// 	if (preg_match("~$uriPattern~", $uri) {
// 		echo $uri;
// 	}
// }
