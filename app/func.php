<?php

//require_once '../vendor/autoload.php';
//use DawPhpPagination\Pagination;

const LIMIT = 10;

// Возваращет сроку запроса для сортировки
function sortUri($sort) {
	$uri = $_SERVER['REQUEST_URI'];
	$uri = explode('?', $uri);
	if (count($uri) > 1)  {
		parse_str($uri[1], $params);
		$params['sort'] = $sort;
		$a = '?'.http_build_query($params);
	} else $a = "?sort=$sort";
	if ($uri[0] == '/search') {
		if (count($uri) > 1)  {
			parse_str($uri[1], $params);
			$params['sort'] = $sort;
			$a = $uri[0];
			$a .= '?'.http_build_query($params);
		}
		
	}
	
	return $a;
}

// Возваращает номер страницы из get параметра
function getPn() {
	if (isset($_GET['pageno']) && is_numeric($_GET['pageno'])) {
    	$pageno = $_GET['pageno'];
	} else {
	    $pageno = 1;
	}
	return $pageno;
}

// Пагинация, возвращает html пагинацию
function pagination($count, $pageno) {
	// if (isset($_GET['pageno']) && is_numeric($_GET['pageno'])) {
 //    	$pageno = $_GET['pageno'];
	// } else {
	//     $pageno = 1;
	// }
	//$no_of_records_per_page = 10;
	//$offset = ($pageno - 1) * LIMIT;
	//$total_pages_sql = getCountAirdrops();
	$total_pages = ceil($count / LIMIT);
	$page = '';
	$prev = '';
	$next = '';

	if ($pageno != 1) {
		$prev = $pageno - 1;
		$prev = "<li class=\"page-item\"><a href=\"?pageno=$prev".searchForPagination().sortForPagination().filterForPagination()."\" class=\"page-link text-dark\">Назад</a></li>";
	}
	if ($pageno >= 1 && $pageno < $total_pages) {
		$next = $pageno + 1;
		$next = "<li class=\"page-item\"><a href=\"?pageno=$next".searchForPagination().sortForPagination().filterForPagination()."\" class=\"page-link text-dark\">Вперед</a></li>";
	}

	for ($i=1; $i <= $total_pages; $i++) {

		if ($pageno == $i) {
			$class = ' active';
		} else $class = '';

		$page .= " <li class=\"$class page-item\"><a href=\"?pageno=$i".searchForPagination().sortForPagination().filterForPagination()."\" class=\"page-link text-dark\">$i</a></li>";
	}

	$render = $prev.$page.$next;
	return $render;
}


// Аирдропы для фильтра
function getAirsByFilter($pageno = 1) {
	global $conn;
	// global $offset;
	// global $no_of_records_per_page;
	// global $render;
	//if (isset($_POST['filter'])) {
		$sortBase = 'ORDER BY';
		$sortCol = 'airdrops.id';
		$limit = LIMIT;
		$offset = ($pageno - 1) * LIMIT;
		$tools = $_GET['tool'];
		$tools1 = [];
		$search2 = '';
		foreach ($tools as $key => $tool) {
			$tools1[] = "tools_airdrops.tool_id = $tool";
		}
		$tools2 = implode(' OR ', $tools1);
		if (isset($_GET['sort'])) {
			validGET();
			$sortCol = $_GET['sort'];
			$sortCol = mysqli_real_escape_string($conn, $sortCol);
		}
		if (isset($_GET['search'])) {
			$search = $_GET['search'];
			$search1 = mysqli_real_escape_string($conn, $search);
			$search2 = "AND airdrops.name LIKE '%$search%'";
		}
		$sql = "SELECT * FROM airdrops LEFT JOIN tools_airdrops ON tools_airdrops.airdrop_id = airdrops.id WHERE $tools2 $search2 GROUP BY airdrops.id $sortBase $sortCol DESC LIMIT $offset, $limit";
		$result = mysqli_query($conn, $sql);
		if (mysqli_num_rows($result) > 0) {
			$rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
			return $rows;
		}
	//}
}

// Количество записей с фильтром
function getCountAirsByFilter() {
	global $conn;
	//if (isset($_POST['filter'])) {
		$tools = $_GET['tool'];
		$tools1 = [];
		$search2 = '';
		foreach ($tools as $key => $tool) {
			$tools1[] = "tools_airdrops.tool_id = $tool";
		}
		$tools2 = implode(' OR ', $tools1);
		if (isset($_GET['search'])) {
			$search = $_GET['search'];
			$search1 = mysqli_real_escape_string($conn, $search);
			$search2 = "AND airdrops.name LIKE '%$search%'";
		}
		$sql = "SELECT COUNT(*) FROM airdrops LEFT JOIN tools_airdrops ON tools_airdrops.airdrop_id = airdrops.id WHERE $tools2 $search2";
		$result = mysqli_query($conn, $sql);
		if (mysqli_num_rows($result) > 0) {
			$rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
			return $rows[0]['COUNT(*)'];
		}
	//}
}

//Активная ссылка меню
function activeMenuLink($item) {
	$menu = [
		'/' => 'Главная',
		'/page/about' => 'О нас',
		'/admin' => 'О нас',
		'/admin/airdrops' => 'О нас',
		'/admin/manage' => 'О нас',
	];

	$uri = explode('?', $_SERVER['REQUEST_URI']);
 	$uri = $uri[0];

 	foreach ($menu as $link => $name) {
 		if ($link == $uri) {
 			//return 'active';
 			return ($uri == $item) ? "active":"";
 		}
 	}

}

// Удаление аирдропа в админке
function delete(){
	global $conn;
	if (isset($_GET['id'])) {
		$id = $_GET['id'];
		$title = 'Удаление аирдропа';
		actionDelete($id);
	}
}

// редактирование аирдропа в админке
function edit() {
	global $conn;
	if (isset($_GET['id'])) {
		$id = $_GET['id'];
		actionUpdate($id);
	}
}

function ajax() {
	global $conn;
	if (isset($_POST) && isset($_SESSION['user'])) {
		$data['user_id'] = $_SESSION['user'];
		$data['id'] = $_POST['id'];
		$data['status'] = $_POST['status'];
		$data['session'] = true;
		if (updateIsDoneById($data['status'], $data['id']) && $data['status'] == 1) {
			createRelUserAir($data['id'], $data['user_id']);
		}
		if (updateIsDoneById($data['status'], $data['id']) && $data['status'] == 0) {
			deleteRelUserAir($data['id'], $data['user_id']);
		}
		echo json_encode($data);
		
	}
}


function adminManage(){
	// global $conn;
	// global $offset;
	// global $no_of_records_per_page;
	// global $render;
	if (checkAdmin() == 'admin') {
		$airdrops = getAllAirdrops(getPn());
		$tools = getAllTools();
		$count = getCountAirdrops();
		$pagination = pagination($count, getPn());
		$title = 'Управление аирдропами';
		$meta_k = '';
		$meta_d = '';
		if (isset($_POST['submit'])) {
			$options['title'] = stripslashes(htmlspecialchars($_POST['title']));
			$options['meta_d'] = stripslashes(htmlspecialchars($_POST['meta_d']));
			$options['meta_k'] = stripslashes(htmlspecialchars($_POST['meta_k']));
			$options['h1'] = stripslashes(htmlspecialchars($_POST['h1']));
			$options['h2'] = stripslashes(htmlspecialchars($_POST['h2']));
			$options['name'] = stripslashes(htmlspecialchars($_POST['name']));
			$options['website'] = stripslashes(htmlspecialchars($_POST['website']));
			$options['ticker'] = stripslashes(htmlspecialchars($_POST['ticker']));
			$options['start_link'] = stripslashes(htmlspecialchars($_POST['start_link']));
			$options['youtube_link'] = stripslashes(htmlspecialchars($_POST['youtube_link']));
			$options['aliace'] = stripslashes(htmlspecialchars($_POST['aliace']));
			$options['num_tokens'] = (int)stripslashes(htmlspecialchars($_POST['num_tokens']));
			$options['fiat_price'] = (int)stripslashes(htmlspecialchars($_POST['fiat_price']));
			//$options['expire_date'] = stripslashes(htmlspecialchars($_POST['date']));
			$options['expire_date'] = (int)strtotime($_POST['date']);
			$options['descr'] = stripslashes(htmlspecialchars($_POST['descr']));
			$options['instruction'] = stripslashes(htmlspecialchars($_POST['instruction']));
			$options['is_done'] = stripslashes(htmlspecialchars($_POST['is_done']));
			$options['is_rec'] = stripslashes(htmlspecialchars($_POST['is_rec']));
			$options['is_ref'] = stripslashes(htmlspecialchars($_POST['is_ref']));
			$options['is_hot'] = stripslashes(htmlspecialchars($_POST['is_hot']));
			$options['is_pin'] = stripslashes(htmlspecialchars($_POST['is_pin']));
			$options['is_new'] = stripslashes(htmlspecialchars($_POST['is_new']));
			$options['logo'] = $options['name'].'.jpg';
			
			if (isset($_POST['req_tools'])) {
				$tools_id = $_POST['req_tools'];
			}
			
			
			$errors = false;

			if (checkAliaceExists($options['aliace'])) {
	            $errors[] = 'Такой алиас уже используется';
	        }
			
			if ($errors == false) {
				$last_id = createAirdrop($options);
				if ($last_id) {
					if (isset($tools_id)) {
						createRelAirTools($last_id, $tools_id);
					}
					
				}
				if (is_uploaded_file($_FILES["logo"]["tmp_name"])) {
		        	$name = $options['name'].'.jpg';
		            // Если загружалось, переместим его в нужную папке, дадим новое имя
		            move_uploaded_file($_FILES["logo"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/img/$name");
		        }
		        //var_dump(createAirdrop($options));
		        //var_dump($_POST);
		        header("Location: /admin/manage");
			}
			// Проверим, загружалось ли через форму изображение
	        
		}
		include '../templates/admin/manage.php';
	} else die('Access denied');
}

function adminAirdrops() {
	global $conn;
	if (checkAdmin() == 'user' OR checkAdmin() == 'admin') {
		if (isset($_SESSION['user'])) {
			$airs = getM2mAirdropsByUser($_SESSION['user']);
			$title = 'Мои аирдропы';
			$meta_k = '';
			$meta_d = '';
		}
		include '../templates/admin/airdrops.php';
	} else die('Access denied');
}

function admin() {
	global $conn;
	if (checkAdmin() == 'user' OR checkAdmin() == 'admin') {
	
		if (isset($_SESSION['user'])) {
			$airs = getM2mAirdropsByUser($_SESSION['user']);
			$title = 'Страница админа';
			$meta_k = '';
			$meta_d = '';
		}
		$sum = 0;
		if ($airs) {
			foreach ($airs as $key => $value) {
				 $sum += $value['fiat_price'];
			}
		}
		$sum = '~'.$sum.'$';
		include '../templates/admin/index.php';
	} else die('Access denied');
	
}

function search() {
	if (isset($_GET['search'])) {
		$title = getPageByRequest('search');
		$title = $title[0]['title'];
		$meta_k = $title[0]['meta_k'];
		$meta_d = $title[0]['meta_d'];
		$tools = getAllTools();
		$recAirs = getRecAirdrops();
		$airs = getSearchAirdrops(getPn());
		$count = getSearchCount($_GET['search']);
		if (isset($_GET['filter'])) {
			$count = getCountAirsByFilter();
			$airs = getAirsByFilter(getPn());
		}
		$pagination = pagination($count, getPn());
		include '../templates/index.php';
	}
}

function airdrop($param) {
	global $conn;
	$recAirs = getRecAirdrops();
	$airdrop = getAirdropByRequest($param);
	$tools = getM2mToolsByAirdrop($airdrop[0]['id']);
	$title = $airdrop[0]['title'];
	$meta_k = $airdrop[0]['meta_k'];
	$meta_d = $airdrop[0]['meta_d'];
	airView($airdrop[0]['id']);
	$check = 0;
	if (isset($_SESSION['user'])) {
		$check = checkRelUserAir($airdrop[0]['id'], $_SESSION['user']);
	}
	include '../templates/airdrop.php';
}

function page($param) {
	global $conn;
	$recAirs = getRecAirdrops();
	$page = getPageByRequest($param);
	$title = $page[0]['title'];
	$meta_k = $page[0]['meta_k'];
	$meta_d = $page[0]['meta_d'];
	include '../templates/page.php';
}

function index() {
	global $conn;
	//global $offset;
	//global $no_of_records_per_page;
	//global $render;
	$tools = getAllTools();
	$recAirs = getRecAirdrops();
	$count = getCountAirdrops();
	$page = getPageByRequest('/');
	$title = $page[0]['title'];
	$meta_k = $page[0]['meta_k'];
	$meta_d = $page[0]['meta_d'];
	if (isset($_GET['filter']) AND !empty($_GET['tool'])) {
		$airs = getAirsByFilter(getPn());
		$count = getCountAirsByFilter();
	}
	if (isset($_GET['filter']) AND empty($_GET['tool'])) {
		$airs = getAllAirdrops(getPn());
	}
	if (!isset($_GET['filter'])) {
		$airs = getAllAirdrops(getPn());
	}
	// $page = 1;
	// if (isset($_GET['pageno'])) {
	// 	$page = $_GET['pageno'];
	// }
	//$airs = getAllAirdrops(getPn());
	$pagination = pagination($count, getPn());
	//$filter = implode('&tool=', $_GET['tool']);
	//var_dump($_GET);

	//else $airs = getAllAirdrops($offset, $no_of_records_per_page);
	include '../templates/index.php';
}

// Вытаскиваем все записи из базы
function getAllAirdrops($pageno = 1) {
	global $conn;
	$sortBase = 'ORDER BY';
	$sortCol = 'id';
	$limit = LIMIT;
	$offset = ($pageno - 1) * LIMIT;
	if (isset($_GET['sort'])) {
		validGET();
		$sortCol = $_GET['sort'];
		$sortCol = mysqli_real_escape_string($conn, $sortCol);
	}
	$sql = "SELECT * FROM airdrops $sortBase $sortCol DESC LIMIT $offset, $limit";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		$rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
		return $rows;
	}	
}

//Рзарешенные GET параметры сортировки
function validGET() {
	$valid = ['views', 'expire_date', 'fiat_price', 'id'];
	// foreach ($_GET as $key => $value) {
	// 	if (!in_array($value, $valid)) {
	// 		header("HTTP/1.0 404 Not Found");
	// 		die();
	// 	}
	// }
	// foreach ($valid as $key => $value) {
	// 	if ($_GET['sort'] == $value) {
	// 		header("HTTP/1.0 404 Not Found");
	//  		die();
	// 	}
	// }
	if(!in_array($_GET['sort'], $valid)) {
		header("HTTP/1.0 404 Not Found");
		die();
	}
}

// Инфо о странице по http запросу
function getPageByRequest($request) {
	global $conn;
	$sql = "SELECT * FROM pages WHERE aliace = '$request'";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		$rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
		return $rows;
	}
}

// Информация об аирдропе
function getAirdropByRequest($request) {
	global $conn;
	$sql = "SELECT * FROM airdrops WHERE aliace = '$request'";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		$rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
		return $rows;
	}
}

// Вытаскиваем записи из базы рекомендуемые
function getRecAirdrops() {
	global $conn;
	$sql = "SELECT * FROM airdrops WHERE is_rec = 1 LIMIT 5";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		$rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
		return $rows;
	}	
}

// Количество всех аирдропов для пагинации
function getCountAirdrops() {
	global $conn;
	$sql = "SELECT COUNT(*) FROM airdrops";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		$rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
		return $rows[0]['COUNT(*)'];
	}
}

// Поиск
function getSearchAirdrops($pageno = 1) {
	global $conn;
	$sortBase = 'ORDER BY';
	$sortCol = 'id';
	$limit = LIMIT;
	$offset = ($pageno - 1) * LIMIT;
	if (isset($_GET['sort'])) {
		validGET();
		$sortCol = $_GET['sort'];
		$sortCol = mysqli_real_escape_string($conn, $sortCol);
	}
	if (isset($_GET['search']) && strlen($_GET['search']) <= 25) {
		$search = $_GET['search'];
		$search = mysqli_real_escape_string($conn, $search);
		$sql = "SELECT * FROM airdrops WHERE name LIKE '%$search%' $sortBase $sortCol DESC LIMIT $offset, $limit";
		$result = mysqli_query($conn, $sql);
		if (mysqli_num_rows($result) > 0) {
			$rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
			return $rows;
		}
	}
}

// Количество всех аирдропов для пагинации для поиска
function getSearchCount($search) {
	global $conn;
	$search = mysqli_real_escape_string($conn, $search);
	$sql = "SELECT COUNT(*) FROM airdrops WHERE name LIKE '%$search%'";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		$rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
		return $rows[0]['COUNT(*)'];
	}
}

// поиск для пагинации
function searchForPagination() {
	global $conn;
	if (isset($_GET['search'])) {
		$search = $_GET['search'];
		$search = mysqli_real_escape_string($conn, $search);
		$search_str = "&search=$search";
		return $search_str;
	}
}
// сортировка для пагинации
function sortForPagination() {
	global $conn;
	if (isset($_GET['sort'])) {
		$sort = $_GET['sort'];
		$sort = mysqli_real_escape_string($conn, $sort);
		$sort_str = "&sort=$sort";
		return $sort_str;
	}
}

// сортировка для пагинации
function filterForPagination() {
	global $conn;
	if (isset($_GET['filter'])) {
		$filter = $_GET['tool'];
		$filter = implode('&tool[]=', $filter);
		//$filter = mysqli_real_escape_string($conn, $filter);
		$filter_str = "&tool[]=".$filter.'&filter=';
		return $filter_str;
	}
}

// Вовзращает интструменты в зависимости от аирдропа
function getM2mToolsByAirdrop($airdrop_id) {
  global $conn;
  $sql = "SELECT * FROM airdrop_tools LEFT JOIN tools_airdrops ON tools_airdrops.tool_id = airdrop_tools.id WHERE tools_airdrops.airdrop_id = '".$airdrop_id."' ORDER BY id DESC";
  $result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		$rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
		return $rows;
	}
}

// Вывод оставшегося время до окончания аирдропа
function expireAirdropTime($time) {
	$days = ceil(($time - time())/60/60/24);
	$aliace = ' дней';
	if ($days <= 3) {
		$days = ceil(($time - time())/60/60);
		$aliace = ' часов';
	}
	$days = ($time < time()) ? '??' : $days;
	return $days.$aliace;
}

// Возвращает все страницы сайта
function getAllPages() {
	global $conn;
	$sql = "SELECT * FROM pages";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		$rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
		return $rows;
	}	
}

// Router (старый)
// function router() {
// 	global $offset;
// 	global $no_of_records_per_page;
// 	global $render;
// 	$uri = explode('?', $_SERVER['REQUEST_URI']);
// 	$uri = $uri[0];
// 	$airdrops = getAllAirdrops($offset, $no_of_records_per_page);
// 	$recAirs = getRecAirdrops();
// 	$pages = getAllPages();
// 	$request = trim($uri, '/');

// 	if ($uri == '/') {
// 		$airs = getAllAirdrops($offset, $no_of_records_per_page);
// 		include '../templates/index.php';
// 	}

// 	foreach ($pages as $key => $page) {
// 		if ($uri == '/'.$page['aliace']) {
// 			include '../templates/page.php';
// 		}
// 	}

// 	if ($airdrops ) {

// 		foreach ($airdrops as $key => $airdrop) {
// 			if ($uri == "/".$airdrop['aliace']) {
// 				$tools = getM2mToolsByAirdrop($airdrop['id']);
// 				//$airdrop = getAirdropByRequest($request);
// 				airView($airdrop['id']);
// 				$check = 0;
// 				if (isset($_SESSION['user'])) {
// 					$check = checkRelUserAir($airdrop['id'], $_SESSION['user']);
// 				}
// 				include '../templates/airdrop.php';
// 			}
// 		}
// 	}

// 	if ($uri == '/search') {
// 		$airs = getSearchAirdrops($offset, $no_of_records_per_page);
// 		include '../templates/index.php';
// 	}

// 	if ($uri == '/login') {
// 		$login = login();
// 	}
// 	if ($uri == '/register') {
// 		register();
// 	}
// 	if ($uri == '/admin') {
// 		checkAdmin();
// 		if (isset($_SESSION['user'])) {
// 			$airs = getM2mAirdropsByUser($_SESSION['user']);
// 		}
// 		$sum = 0;
// 		if ($airs) {
// 			foreach ($airs as $key => $value) {
// 				 $sum += $value['fiat_price'];
// 			}
// 		}
// 		$sum = '~'.$sum.'$';
// 		include '../templates/admin/index.php';
// 	}
// 	if ($uri == '/admin/logout') {
// 		logout();
// 	}
// 	if ($uri == '/admin/airdrops') {
// 		checkAdmin();
// 		if (isset($_SESSION['user'])) {
// 			$airs = getM2mAirdropsByUser($_SESSION['user']);
// 		}
// 		include '../templates/admin/airdrops.php';
// 	}
// 	if ($uri == '/admin/manage') {
// 		checkAdmin();
// 		$tools = getAllTools();
// 		include '../templates/admin/manage.php';
// 	}
// 	if ($uri == '/ajax') {
		
// 		if (isset($_POST) && isset($_SESSION['user'])) {
// 			$data['user_id'] = $_SESSION['user'];
// 			$data['id'] = $_POST['id'];
// 			$data['status'] = $_POST['status'];
// 			$data['session'] = true;
// 			if (updateIsDoneById($data['status'], $data['id']) && $data['status'] == 1) {
// 				createRelUserAir($data['id'], $data['user_id']);
// 			}
// 			if (updateIsDoneById($data['status'], $data['id']) && $data['status'] == 0) {
// 				deleteRelUserAir($data['id'], $data['user_id']);
// 			}
// 			echo json_encode($data);
			
// 		}
		
// 	}
// 	if ($uri == '/addair') {
// 		addAir();
// 	}
// 	if ($uri == '/edit') {
// 		if (isset($_GET['id'])) {
// 			$id = $_GET['id'];
// 			actionUpdate($id);
// 		}
// 	}
// 	if ($uri == '/delete') {
// 		if (isset($_GET['id'])) {
// 			$id = $_GET['id'];
// 			actionDelete($id);
// 		}
// 	}
// 	if ($uri == '/addNote') {
// 		addNoteAjax();
// 	}
// 	if ($uri == '/status') {
// 		changeStatusAjax();
// 	}
// 	if ($uri == '/delDrop') {
// 		delDrop();
// 	}
// 	if ($uri == false) {
// 		echo 'Не найдено';
// 	}
// }

// вход на сайт
function login() {
	global $conn;
	$name = '';
	$pass = '';
	$auth = false;
	$title = getPageByRequest('login');
	$title = $title[0]['title'];
	$meta_k = '';
	$meta_d = '';

	if (isset($_POST['submit'])) {
		$name = mysqli_real_escape_string($conn, stripslashes(htmlspecialchars($_POST['name'])));
		$pass = mysqli_real_escape_string($conn, stripslashes(htmlspecialchars($_POST['pass'])));

		$errors = false;

		if (!checkName($name)) {
            $errors[] = 'Неверное имя или пароль';
        }
        if (!checkPass($pass)) {
            $errors[] = 'Неверное имя или пароль';
        }

        $userId = checkUserData($name);
        $userPass = checkUserPass($name);

		if ($userId == false or $userPass == false) {
			$errors[] = 'Неправильные данные для входа на сайт';
		}

		if (password_verify($pass, $userPass)) {
			auth($userId);
			header("Location: admin");
		} else {
			$errors[] = 'Неправильные данные для входа на сайт';
		}

		//  else {
		// 	auth($userId);
		// 	header("Location: admin");
		// }
		
	}
	include '../templates/user/login.php';
	//return true;
}

// проверка имени
function checkName($name) {
	if (strlen($name) >= 2) {
        return true;
    }
    return false;
}

// Проверка пароля
function checkPass($password) {
    if (strlen($password) >= 6) {
        return true;
    }
    return false;
}

// Проверка на существование пользователя
function checkUserData($name) {
    global $conn;
	$sql = "SELECT * FROM user WHERE name = '$name'";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		$rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
		return $rows[0]['id'];
	}	
}
// хэш пароля пользователя
function checkUserPass($name) {
    global $conn;
	$sql = "SELECT * FROM user WHERE name = '$name'";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		$rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
		return $rows[0]['pass'];
	}	
}

// записываем юзера в сессию
function auth($userId) {
	session_start();
    // Записываем идентификатор пользователя в сессию
    $_SESSION['user'] = $userId;
}

// Проверяем является ли пользователь администратором
function checkAdmin(){
    // Проверяем авторизирован ли пользователь. Если нет, он будет переадресован
    $userId = checkLogged();

    // Получаем информацию о текущем пользователе
    $user = getUserById($userId);

    // Если роль текущего пользователя "admin", пускаем его в админпанель
    // if ($user[0]['role'] == 'admin') {
    //     return true;
    // }

    return $user[0]['role'];

    // Иначе завершаем работу с сообщением об закрытом доступе
    //die('Access denied');
}



// Возвращает идентификатор пользователя, если он авторизирован
// Иначе перенаправляет на страницу входа
function checkLogged() {
	//session_start();
    // Если сессия есть, вернем идентификатор пользователя
    if (isset($_SESSION['user'])) {
        return $_SESSION['user'];
    } else header("Location: /login");

    
}

// Возвращает юзера по id
function getUserById($id) {
	global $conn;
	$sql = "SELECT * FROM user WHERE id = '$id'";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		$rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
		return $rows;
	}	
}

// Регистрация
function register() {
	global $conn;
	$name = false;
	$pass = false;
	$result = false;
	$title = getPageByRequest('register');
	$title = $title[0]['title'];
	$meta_k = '';
	$meta_d = '';

	if (isset($_POST['submit'])) {
		$name = mysqli_real_escape_string($conn, stripslashes(htmlspecialchars($_POST['name'])));
		$pass = mysqli_real_escape_string($conn, stripslashes(htmlspecialchars($_POST['pass'])));

		$errors = false;

		if (!checkName($name)) {
            $errors[] = 'Имя не должно быть короче 2-х символов';
        }
        if (!checkPass($pass)) {
            $errors[] = 'Пароль не должен быть короче 6-ти символов';
        }
        if (checkEmailExists($name)) {
            $errors[] = 'Такой email уже используется';
        }
        if (isset($_POST['pass']) && $_POST['pass'] !== $_POST['passConf']) {
	        $errors[] = 'Пароли не совпадают';
	    }
	    $pass = password_hash($pass, PASSWORD_DEFAULT);

        if ($errors == false) {
            // Если ошибок нет
            // Регистрируем пользователя
            $result = registerUser($name, $pass);
            $success = 'Вы успешно зарегистрировались!';
        }
        
	}
	// Подключаем вид
        include '../templates/user/register.php';
        return true;
}


// проверка на существование емаила
function checkEmailExists($name) {
    global $conn;
	$sql = "SELECT COUNT(*) FROM user WHERE name = '$name'";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		$rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
		return $rows[0]['COUNT(*)'];
	}
}

// проверка на существование алиаса
function checkAliaceExists($aliace) {
    global $conn;
	$sql = "SELECT COUNT(*) FROM airdrops WHERE aliace = '$aliace'";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		$rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
		return $rows[0]['COUNT(*)'];
	}
}

function registerUser($name, $pass) {
	global $conn;
	$sql = "INSERT INTO user (name, pass, role) VALUES ('$name', '$pass', 'user')";

	if (mysqli_query($conn, $sql)) {
		return true;
	} else {
		$error = mysqli_error($conn);
		return $error;
	}
}

// выход из админки
function logout() {
    // Стартуем сессию
    session_start();
    
    // Удаляем информацию о пользователе из сессии
    unset($_SESSION["user"]);
    
    // Перенаправляем пользователя на главную страницу
    header("Location: /login");
}

// Вовзращает аирдропы в зависимости от юзера
function getM2mAirdropsByUser($user_id) {
  global $conn;
  $sql = "SELECT * FROM airdrops LEFT JOIN user_airdrops ON user_airdrops.airdrop_id = airdrops.id WHERE user_airdrops.user_id = '".$user_id."' ORDER BY id DESC";
  $result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		$rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
		return $rows;
	}
}

// обновление статуса выполненного аирдропа
function updateIsDoneById($status, $id) {
	global $conn;
	$sql = "UPDATE airdrops SET is_done = '$status' WHERE id = '$id'";
	if (mysqli_query($conn, $sql)) {
		return true;
	} else {
		$error = mysqli_error($conn);
		return $error;
	}
}

// добавление связи юзер-аирдроп
function createRelUserAir($aridrop_id, $user_id) {
	global $conn;
	$sql = "INSERT INTO user_airdrops (airdrop_id, user_id) VALUES ('$aridrop_id', '$user_id')";
	if (mysqli_query($conn, $sql)) {
		return true;
	} else {
		$error = mysqli_error($conn);
		return $error;
	}
}

// удаление связи юзер-аирдроп
function deleteRelUserAir($aridrop_id, $user_id) {
	global $conn;
	$sql = "DELETE FROM user_airdrops WHERE airdrop_id = '$aridrop_id' AND user_id = '$user_id'";
	if (mysqli_query($conn, $sql)) {
		return true;
	} else {
		$error = mysqli_error($conn);
		return $error;
	}
}

// проверка наличия связи юзер+аирдроп
function checkRelUserAir($aridrop_id, $user_id) {
	global $conn;
	$sql = "SELECT * FROM user_airdrops WHERE airdrop_id = '$aridrop_id' AND user_id = '$user_id'";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		$rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
		return true;
	}
}

// Создание аирдропа
function createAirdrop($options) {
	global $conn;
	//$options['expire_date'] = strtotime($options['expire_date']);
    $columns = implode(', ', array_keys($options));
	$escaped_values = array_map(array($conn, 'real_escape_string'), $options);
	$values  = implode("', '", $escaped_values);
	//$sql = "INSERT INTO `fbdata`($columns) VALUES ($values)";
    //$values  = implode(', ', array_values($options));
    // $name = $options['name'];
    // $aliace = $options['aliace'];
    // $num_tokens = $options['num_tokens'];
    // $fiat_price = $options['fiat_price'];
    // $expire_date = strtotime($_POST['date']);
    // $views = $options['views'];
    // $descr = $options['descr'];
    // $instruction = $options['instruction'];
    // $is_done = $options['is_done'];
    // $is_rec = $options['is_rec'];
    // $is_ref = $options['is_ref'];


	//$sql = "INSERT INTO airdrops ($columns) VALUES ('$name', '$aliace', '$num_tokens', '$fiat_price', '$expire_date', '$views', '$descr', '$instruction', '$is_done', $is_rec, '$is_ref')";
	$sql = "INSERT INTO airdrops ($columns) VALUES ('$values')";
	if (mysqli_query($conn, $sql)) {
		$last_id = mysqli_insert_id($conn);
		return $last_id;
	} else {
		$error = mysqli_error($conn);
		return $error;
	}
}

// Возвращает все инструменты для аирдропов
function getAllTools() {
	global $conn;
	$sql = "SELECT * FROM airdrop_tools";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		$rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
		return $rows;
	}
}

// Преобразование многострочного текста в список
function textToLi($text) {
	$newtext = explode("\n", $text);
	$li = '';
	foreach ($newtext as $key => $value) {
		$li .= "<li class=\"list-group-item pl-0\"><i class=\"fas fa-arrow-alt-circle-right\"></i> $value</li>";
	}
	return $li;
}

// Обработка формы и создание аирдропа
function addAir() {
	global $conn;
	if (isset($_POST['submit'])) {
		$options['title'] = stripslashes(htmlspecialchars($_POST['title']));
		$options['meta_d'] = stripslashes(htmlspecialchars($_POST['meta_d']));
		$options['meta_k'] = stripslashes(htmlspecialchars($_POST['meta_k']));
		$options['h1'] = stripslashes(htmlspecialchars($_POST['h1']));
		$options['h2'] = stripslashes(htmlspecialchars($_POST['h2']));
		$options['name'] = stripslashes(htmlspecialchars($_POST['name']));
		$options['website'] = stripslashes(htmlspecialchars($_POST['website']));
		$options['ticker'] = stripslashes(htmlspecialchars($_POST['ticker']));
		$options['start_link'] = stripslashes(htmlspecialchars($_POST['start_link']));
		$options['aliace'] = stripslashes(htmlspecialchars($_POST['aliace']));
		$options['num_tokens'] = (int)stripslashes(htmlspecialchars($_POST['num_tokens']));
		$options['fiat_price'] = (int)stripslashes(htmlspecialchars($_POST['fiat_price']));
		//$options['expire_date'] = stripslashes(htmlspecialchars($_POST['date']));
		$options['expire_date'] = (int)strtotime($_POST['date']);
		$options['descr'] = stripslashes(htmlspecialchars($_POST['descr']));
		$options['instruction'] = stripslashes(htmlspecialchars($_POST['instruction']));
		$options['is_done'] = stripslashes(htmlspecialchars($_POST['is_done']));
		$options['is_rec'] = stripslashes(htmlspecialchars($_POST['is_rec']));
		$options['is_ref'] = stripslashes(htmlspecialchars($_POST['is_ref']));
		$options['is_hot'] = stripslashes(htmlspecialchars($_POST['is_hot']));
		$options['is_pin'] = stripslashes(htmlspecialchars($_POST['is_pin']));
		$options['is_new'] = stripslashes(htmlspecialchars($_POST['is_new']));
		$options['logo'] = $options['name'].'.jpg';
		
		if (isset($_POST['req_tools'])) {
			$tools_id = $_POST['req_tools'];
		}
		
		
		$errors = false;

		if (checkAliaceExists($options['aliace'])) {
            $errors[] = 'Такоq алиас уже используется';
        }
		
		if ($errors == false) {
			$last_id = createAirdrop($options);
			if ($last_id) {
				if (isset($tools_id)) {
					createRelAirTools($last_id, $tools_id);
				}
				
			}
			if (is_uploaded_file($_FILES["logo"]["tmp_name"])) {
	        	$name = $options['name'].'.jpg';
	            // Если загружалось, переместим его в нужную папке, дадим новое имя
	            move_uploaded_file($_FILES["logo"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/img/$name");
	        }
	        //var_dump(createAirdrop($options));
	        //var_dump($_POST);
	        header("Location: /admin/manage");
		}
		// Проверим, загружалось ли через форму изображение
        
	}
	if (!isset($_POST['submit'])) {
		exit('Ошибка');
	}
	include '../templates/admin/manage.php';
    return true;
}

//Добавление новых записей в таблицу отношений
function createRelAirTools($air_id, $tools_id) {
  	global $conn;

    $rows = [];

    foreach ($tools_id as $value) {
        $rows[] = [
        		'airdrop_id' => $air_id,
                'tool_id' => $value
              ];
    }
    foreach ($rows as $key => $value) {
    	$str[] = '('.$value['airdrop_id'].', '.$value['tool_id'].')';
    }

    //$ids = array_column($rows, 'tool_id');
	//$output = implode(',', $ids);

    //$columns = implode(', ', array_keys($rows));
    $values  = implode(", ", array_values($str));

    // Текст запроса к БД
    $sql = "INSERT INTO tools_airdrops (airdrop_id, tool_id) VALUES $values";

    if (mysqli_query($conn, $sql)) {
		return true;
	} else {
		$error = mysqli_error($conn);
		return $error;
	}
}

//Удаление записей о инструментах в таблицу отношений
function deleteRelAirTools($air_id) {
  	global $conn;

    // Текст запроса к БД
    $sql = "DELETE FROM tools_airdrops WHERE airdrop_id = $air_id";

    if (mysqli_query($conn, $sql)) {
		return true;
	} else {
		$error = mysqli_error($conn);
		return $error;
	}
}

// Удаление аирдропа
function deleteAirById($air_id) {
	global $conn;
	$sql = "DELETE FROM airdrops WHERE id = '$air_id'";
	if (mysqli_query($conn, $sql)) {
		return true;
	} else {
		$error = mysqli_error($conn);
		return $error;
	}
}

// action удалить
function actionDelete($air_id) {
	if (checkAdmin() == 'admin') {
		$title = 'Удаление аирдропа';
		$meta_k = '';
		$meta_d = '';
		if (isset($_POST['submit'])) {
			deleteAirById($air_id);
			header("Location: /admin/manage");
		}
		include '../templates/admin/delete_air.php';
	} else die('Access denied');
}

// action обновить
function actionUpdate($air_id) {
	global $conn;
	if (checkAdmin() == 'admin') {
	$title = 'Редактрование аирдропа';
	$meta_k = '';
	$meta_d = '';
	$airdrop = getAirdropById($air_id);
	$tool_rel = getM2mToolsByAirdrop($air_id);
	$tools = getAllTools();
	if (isset($_POST['submit'])) {
		$options['title'] = stripslashes(htmlspecialchars($_POST['title']));
		$options['meta_d'] = stripslashes(htmlspecialchars($_POST['meta_d']));
		$options['meta_k'] = stripslashes(htmlspecialchars($_POST['meta_k']));
		$options['h1'] = stripslashes(htmlspecialchars($_POST['h1']));
		$options['h2'] = stripslashes(htmlspecialchars($_POST['h2']));
		$options['name'] = stripslashes(htmlspecialchars($_POST['name']));
		$options['website'] = stripslashes(htmlspecialchars($_POST['website']));
		$options['ticker'] = stripslashes(htmlspecialchars($_POST['ticker']));
		$options['start_link'] = stripslashes(htmlspecialchars($_POST['start_link']));
		$options['youtube_link'] = stripslashes(htmlspecialchars($_POST['youtube_link']));
		$options['aliace'] = stripslashes(htmlspecialchars($_POST['aliace']));
		$options['num_tokens'] = stripslashes(htmlspecialchars($_POST['num_tokens']));
		$options['fiat_price'] = stripslashes(htmlspecialchars($_POST['fiat_price']));
		$options['expire_date'] = stripslashes(htmlspecialchars($_POST['date']));
		$options['descr'] = stripslashes(htmlspecialchars($_POST['descr']));
		$options['instruction'] = stripslashes(htmlspecialchars($_POST['instruction']));
		$options['is_done'] = stripslashes(htmlspecialchars($_POST['is_done']));
		$options['is_rec'] = stripslashes(htmlspecialchars($_POST['is_rec']));
		$options['is_ref'] = stripslashes(htmlspecialchars($_POST['is_ref']));
		$options['is_hot'] = stripslashes(htmlspecialchars($_POST['is_hot']));
		$options['is_pin'] = stripslashes(htmlspecialchars($_POST['is_pin']));
		$options['is_new'] = stripslashes(htmlspecialchars($_POST['is_new']));
		$options['logo'] = $options['name'].'.jpg';
		
		$tools_id = $_POST['req_tools'];
		
		

		$errors = false;
		
		if ($errors == false) {
			updateAirById($air_id, $options);
			if ($tools_id) {
                    deleteRelAirTools($air_id);
                    createRelAirTools($air_id, $tools_id);
                } elseif (empty($category_id)) {
                    deleteRelAirTools($air_id);
                }
			if (is_uploaded_file($_FILES["logo"]["tmp_name"])) {
	        	$name = $options['name'].'.jpg';
	            // Если загружалось, переместим его в нужную папке, дадим новое имя
	            move_uploaded_file($_FILES["logo"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/img/$name");
	        }
	        //var_dump(updateAirById($air_id, $options));
	        header("Location: /admin/manage");
	}
	
}
include '../templates/admin/update_air.php';
} else die('Access denied');
}

// модель обновить аирдроп
function updateAirById($id, $options) {
	global $conn;
	$title = mysqli_real_escape_string($conn, $options['title']);
	$meta_d = mysqli_real_escape_string($conn, $options['meta_d']);
	$meta_k = mysqli_real_escape_string($conn, $options['meta_k']);
	$h1 = mysqli_real_escape_string($conn, $options['h1']);
	$h2 = mysqli_real_escape_string($conn, $options['h2']);
	$name = mysqli_real_escape_string($conn, $options['name']);
	$website = mysqli_real_escape_string($conn, $options['website']);
	$ticker = mysqli_real_escape_string($conn, $options['ticker']);
	$start_link = mysqli_real_escape_string($conn, $options['start_link']);
	$youtube_link = mysqli_real_escape_string($conn, $options['youtube_link']);
	$aliace = mysqli_real_escape_string($conn, $options['aliace']);
	$num_tokens = mysqli_real_escape_string($conn, $options['num_tokens']);
	$fiat_price = mysqli_real_escape_string($conn, $options['fiat_price']);
	$expire_date = mysqli_real_escape_string($conn, strtotime($options['expire_date']));
	$descr = mysqli_real_escape_string($conn, $options['descr']);
	$instruction = mysqli_real_escape_string($conn, $options['instruction']);
	$is_done = mysqli_real_escape_string($conn, $options['is_done']);
	$is_rec = mysqli_real_escape_string($conn, $options['is_rec']);
	$is_ref = mysqli_real_escape_string($conn, $options['is_ref']);
	$is_hot = mysqli_real_escape_string($conn, $options['is_hot']);
	$is_pin = mysqli_real_escape_string($conn, $options['is_pin']);
	$is_new = mysqli_real_escape_string($conn, $options['is_new']);
	$logo = $name.'.jpg';

    

	$sql = "UPDATE airdrops SET title = '$title', meta_d = '$meta_d', meta_k = '$meta_k', h1 = '$h1', h2 = '$h2', logo = '$logo', name = '$name', ticker = '$ticker', aliace ='$aliace', website = '$website', start_link = '$start_link', youtube_link = '$youtube_link',  num_tokens = '$num_tokens', fiat_price = '$fiat_price', expire_date = '$expire_date', descr = '$descr', instruction = '$instruction', is_done = '$is_done', is_rec = '$is_rec', is_ref = '$is_ref', is_hot = '$is_hot', is_pin = '$is_pin', is_new = '$is_new' WHERE id = '$id'";

	if (mysqli_query($conn, $sql)) {
		return true;
	} else {
		$error = mysqli_error($conn);
		return $error;
	}
}

// Возврашает аирдроп по id
function getAirdropById($id) {
	global $conn;
	$sql = "SELECT * FROM airdrops WHERE id = '$id'";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		$rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
		return $rows;
	}	
}

// Возвращает путь к изображению
function getImage($id) {
	// Название изображения-пустышки
    $noImage = 'no-image.jpg';

    // Путь к папке с товарами
    $path = '/img/';

    // Путь к изображению товара
    $pathToProductImage = $path . $id . '.jpg';

    if (file_exists($_SERVER['DOCUMENT_ROOT'].$pathToProductImage)) {
        // Если изображение для товара существует
        // Возвращаем путь изображения товара
        return $pathToProductImage;
    }

    // Возвращаем путь изображения-пустышки
    return $path . $noImage;
}

// Возвращает заметки по id юзера
function getNotesByRelUserAir($user_id, $air_id) {
	global $conn;
	$sql = "SELECT * FROM user_airdrops WHERE user_id = '$user_id' AND airdrop_id = '$air_id'";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		$rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
		return $rows[0]['note'];
	}	
}

// Добаление заметки к аирдропу асинхронно
function addNoteAjax() {
	if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		global $conn;
		$user_id = $_POST['user_id'];
		$airdrop_id = $_POST['airdrop_id'];
		$note = $_POST['note'];
		if (addNote($user_id, $airdrop_id, $note)) {
			return true;
		}
		exit();
	}
	echo "Это НЕ аякс";
}

// Добавление заметки юзера к аирдоопу
function addNote($id, $air, $note) {
	global $conn;
	$sql = "UPDATE user_airdrops SET note = '$note' WHERE user_id = '$id' AND airdrop_id = '$air'";

	if (mysqli_query($conn, $sql)) {
		return true;
	} else {
		$error = mysqli_error($conn);
		return $error;
	}
}

// Смена статуса выплачен/не выплачен
function changeStatus($id, $air, $status) {
	global $conn;
	$sql = "UPDATE user_airdrops SET is_paid = '$status' WHERE user_id = '$id' AND airdrop_id = '$air'";

	if (mysqli_query($conn, $sql)) {
		return true;
	} else {
		$error = mysqli_error($conn);
		return $error;
	}
}

// Возвращает статус выплачен/не выплачен
function getStatusByRelUserAir($user_id, $air_id) {
	global $conn;
	$sql = "SELECT * FROM user_airdrops WHERE user_id = '$user_id' AND airdrop_id = '$air_id'";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		$rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
		return $rows[0]['is_paid'];
	}	
}

// Смена статуса аирдропа
function changeStatusAjax() {
	if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		global $conn;
		$user_id = $_POST['user_id'];
		$airdrop_id = $_POST['airdrop_id'];
		$status = $_POST['status'];
		if (changeStatus($user_id, $airdrop_id, $status)) {
			return true;
		}
		exit();
	}
	echo "Это НЕ аякс";
}


// Удаление аирдропа на странице с выполненными ajax
function delDrop() {
	if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		global $conn;
		$user = $_POST['user_id'];
		$airdrop = $_POST['airdrop_id'];
		if (deleteRelUserAir($airdrop, $user)) {
			return true;
		}
		exit();
	}
	echo "Это НЕ аякс";
}

// счетчик просмотров страницы аирдропа
function airView($id) {
	if (!isset($_SESSION['view'.$id])) {
		updateViewById($id);
		$_SESSION['view'.$id] = 1;
	}
}

// Обновление количества просмотров на странице аирдропа
function updateViewById($id) {
	global $conn;
	$sql = "UPDATE airdrops SET views = views + 1 WHERE id = '$id'";
	if (mysqli_query($conn, $sql)) {
		return true;
	} else {
		$error = mysqli_error($conn);
		return $error;
	}
}

// Получение информации для модального окна ajax
function getDrop(){
	$id = $_POST['id'];
	global $conn;
	$data = getAirdropById($id);
	$tool_rel = getM2mToolsByAirdrop($id);
	$data[] = $tool_rel;

	echo json_encode($data);
}

// Текст для сортировки
function textToSort(){
	$arr = [
			'views' => 'Просмотры',
			'expire_date' => 'Дата окончания',
			'fiat_price' => 'Цена'
		];
	$str = 'Дата добавления';
	if (isset($_GET['sort'])) {
		switch ($_GET['sort']) {
			case 'views':
				$str = $arr['views'];
				break;
			case 'expire_date':
				$str = $arr['expire_date'];
				break;
			case 'fiat_price':
				$str = $arr['fiat_price'];
				break;

			default:
				$str = 'Дата добавления';
				break;
		}
	}
	return $str;
}




