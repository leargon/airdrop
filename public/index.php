<?php

session_start();

require '../app/db.php';
require '../app/func.php';
//require '../app/pagination.php';
require '../app/AltoRouter.php';

$router = new AltoRouter();

//$router->map( 'GET', '/airdrop/[a:action]', 'airdrop');
$router->addRoutes([
	  ['GET', '/airdrop/[a:action]', 'airdrop'],
	  ['GET', '/page/[a:action]', 'page'],
	  ['GET', '/search', 'search'],
	  ['POST', '/login', 'login'],
	  ['GET', '/login', 'login'],
	  ['GET', '/register', 'register'],
	  ['POST', '/register', 'register'],
	  ['GET', '/admin/logout', 'logout'],
	  ['GET', '/admin', 'admin'],
	  ['GET', '/admin/airdrops', 'adminAirdrops'],
	  ['GET', '/admin/manage', 'adminManage'],
	  ['POST', '/admin/manage', 'adminManage'],
	  ['POST', '/ajax', 'ajax'],
	  ['POST', '/addair', 'addAir'],
	  ['GET', '/edit', 'edit'],
	  ['POST', '/edit', 'edit'],
	  ['GET', '/delete', 'delete'],
	  ['POST', '/delete', 'delete'],
	  ['POST', '/addNote', 'addNoteAjax'],
	  ['POST', '/status', 'changeStatusAjax'],
	  ['POST', '/delDrop', 'delDrop'],
	  ['POST', '/getdrop', 'getDrop'],
	  ['GET', '/', 'index'],
	  ['POST', '/', 'index']
  ]);

$match = $router->match();

// do we have a match?
// call closure or throw 404 status
if( is_array($match) && is_callable( $match['target'] ) ) {
	call_user_func_array( $match['target'], $match['params'] ); 
} else {
	// no route was matched
	header( $_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
}


