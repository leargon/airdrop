<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'DawPhpPagination\\' => array($vendorDir . '/stephweb/daw-php-pagination/src/DawPhpPagination'),
);
