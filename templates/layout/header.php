<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="keywords" content="<?=$meta_k?>">
    <meta name="description" content="<?=$meta_d?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title><?=$title?></title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
	<style>
		.square {
			width: 100px;
			height: 100px;
		}

		.drop {
			outline: 1px solid black;
		}
		.drop:hover {
			outline: 1px solid #FF9400;
		}
		.bg {
			/*background-image: url(img/concrete_seamless.png);*/
			background:url("/img/concrete_seamless.png") repeat;
		}
		/* The switch - the box around the slider */
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

/* Hide default HTML checkbox */
.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

/* The slider */
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
html, body {
	height: 100%; min-height: 100%;
	}
	</style>

</head>
<body class="bg">
	<!-- верхнее меню -->
	<!-- <div class="container">
		<h3 class="bg-dark text-white p-1">Топ меню</h3>
	</div> -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
		<div class="container">
		<!-- <a class="navbar-brand" href="#">Navbar</a> -->
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
			
			<ul class="navbar-nav mr-auto">
				<li class="nav-item">
					<a class="nav-link <?=activeMenuLink('/')?>" href="/">Главная <span class="sr-only">(current)</span></a>
				</li>
				<!-- <li class="nav-item">
					<a class="nav-link" href="#">Закрытые</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Биржи</a>
				</li> -->
				<li class="nav-item">
					<a class="nav-link <?=activeMenuLink('/page/about')?>" href="/page/about">О нас</a>
				</li>
			</ul>
			<ul class="navbar-nav">
				
				<?php if (isset($_SESSION['user'])): ?>

	                <li class="nav-item">
	                    <a class="nav-link <?=activeMenuLink('/admin')?>" href="/admin">Панель администратора</a>
	                </li>
	                <li class="nav-item">
	                    <a class="nav-link <?=activeMenuLink('/admin/airdrops')?>" href="/admin/airdrops">Мои аирдропы</a>
	                </li>

	                <?php if (checkAdmin() == 'admin'): ?>

		                <li class="nav-item">
		                    <a class="nav-link <?=activeMenuLink('/admin/manage')?>" href="/admin/manage">Управление аирдропами</a>
		                </li>

					<?php endif; ?>

	                <li class="nav-item">
	                    <a class="nav-link" href="/admin/logout">Выход</a>
	                </li>

				<?php endif; ?>

				<?php if (!isset($_SESSION['user'])): ?>
				
		                <li class="nav-item">
		                    <a class="nav-link" href="/login">Вход</a>
		                </li>
		                <li class="nav-item">
		                    <a class="nav-link" href="/register">Регистрация</a>
		                </li>

                <?php endif; ?>
            </ul>
			
		</div>
		</div>
	</nav>

	<!-- шапка -->
	<div class="container-fluid shadow p-1 bg-white mt-5">
		<div class="container">
			<div class="row align-items-center h-100">
				<div class="col-lg-3">
					<a href="/"><img src="/img/logo.png" class="img-fluid" alt="Responsive image"></a>
				</div>
				<div class="col-lg-6 mx-auto">
					<form action="/search" method="GET">
						<div class="input-group">
						  <input name="search" type="text" class="form-control" placeholder="Поиск" required="">
						  <div class="input-group-append">
						    <button class="btn btn-outline-secondary" type="submit">Искать</button>
						  </div>
						</div>
					</form>
				</div>
				<div class="col-lg-3">
					<ul class="list-inline float-right mb-0" style="z-index: 1000">
					   <!-- <li class="list-inline-item"><a class="social-icon text-xs-center" target="_blank" href="#" style="z-index: 11"><i class="fa-3x fab fa-vk"></i></a></li> -->
					   <li class="list-inline-item" style="z-index: 1000"><a class="social-icon text-xs-center" target="_blank" href="https://www.youtube.com/channel/UCJiCOvnk-Ps65jClBD8MZsA?view_as=subscriber" ><i class="fa-3x fab fa-youtube" style="z-index: 1000"></i></a></li>
					   <li class="list-inline-item"><a class="social-icon text-xs-center" target="_blank" href="https://t.me/airolejon"><i class="fa-3x fab fa-telegram"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>