<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Document</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
	<style>
		.drop {
			outline: 1px solid black;
		}
		.drop:hover {
			outline: 1px solid #FF9400;
		}
		.bg {
			/*background-image: url(img/concrete_seamless.png);*/
			background:url("img/concrete_seamless.png") repeat;
		}
	</style>
</head>
<body class="bg">
	<!-- верхнее меню -->
	<!-- <div class="container">
		<h3 class="bg-dark text-white p-1">Топ меню</h3>
	</div> -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<!-- <a class="navbar-brand" href="#">Navbar</a> -->
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
			<div class="container">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href="/">Главная <span class="sr-only">(current)</span></a>
				</li>
				<!-- <li class="nav-item">
					<a class="nav-link" href="#">Закрытые</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Биржи</a>
				</li> -->
				<li class="nav-item">
					<a class="nav-link" href="/about">О нас</a>
				</li>
			</ul>
			</div>
		</div>
	</nav>

	<!-- шапка -->
	<div class="container-fluid h-100 shadow p-1 bg-white">
		<div class="container">
			<div class="row align-items-center h-100">
				<div class="col-lg-3">
					<a href="/"><img src="img/logo.png" class="img-fluid" alt="Responsive image"></a>
				</div>
				<div class="col-lg-6 mx-auto">
					<form action="/search" method="GET">
						<div class="input-group">
						  <input name="search" type="text" class="form-control" placeholder="Поиск" required="">
						  <div class="input-group-append">
						    <button class="btn btn-outline-secondary" type="submit">Искать</button>
						  </div>
						</div>
					</form>
				</div>
				<div class="col-lg-3">
					<ul class="list-inline float-right mb-0">
					   <li class="list-inline-item"><a class="social-icon text-xs-center" target="_blank" href="#"><i class="fa-3x fab fa-vk"></i></a></li>
					   <li class="list-inline-item"><a class="social-icon text-xs-center" target="_blank" href="#"><i class="fa-3x fab fa-youtube"></i></a></li>
					   <li class="list-inline-item"><a class="social-icon text-xs-center" target="_blank" href="#"><i class="fa-3x fab fa-telegram"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>