<div class="col-lg-4 pl-0 order-2 order-sm-1">
	<div class="card bg-white shadow">
		<div class="card-header bg-white">
	    <span class="h5">Рекомендуемые аирдропы</span>
	  </div>
	  <ul class="list-group list-group-flush">

		<?php foreach ($recAirs as $key => $air): ?>

	    <li class="list-group-item">
				<div class="row align-items-center pl-2">
					<div class="col-lg-3 col-4">
						<img src="<?=getImage($air['name']);?>" alt="" class="img-fluid">
					</div>
					<div class="col-lg-9 col-8">
						<h6><a href="/airdrop/<?=$air['aliace']?>" style="color: black"><?=$air['name']?></a></h6>
						<h6><i class="far fa-money-bill-alt" style="color: #00F900"></i> <?=$air['fiat_price']?>$</h6>
					</div>
				</div>
	    </li>

		<?php endforeach; ?>

	  </ul>
	</div>
</div>