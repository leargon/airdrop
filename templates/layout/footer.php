<!-- подвал -->
	<div class="container p-4 bg-dark ">
		<!-- <h3 class="bg-dark text-white p-4">Подвал</h3> -->
		<ul class="nav justify-content-center">
		  <!-- <li class="nav-item">
		    <a class="nav-link text-white"  href="#">Партнеры</a>
		  </li> -->
		  <li class="nav-item">
		    <a class="nav-link text-white" href="/page/rules">Правила использования</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link text-white" href="/page/support">Поддержка</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link text-white" href="/page/disclaimer" tabindex="-1" aria-disabled="true">Отказ от ответственности</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link text-white" href="/page/politics" tabindex="-1" aria-disabled="true">Политика конфиденциальности</a>
		  </li>
		</ul>
	</div>

	<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Вы должны быть авторизованы для использования данной функции</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!-- <div class="modal-body">
        ...
      </div> -->
      <div class="modal-footer">
        <a href="/login" role="button" class="btn btn-secondary">Вход</a>
        <a href="/register" role="button" class="btn btn-primary">Регистрация</a>
      </div>
    </div>
  </div>
</div>
<div style="position: fixed; top: 60px; right: 0; z-index: -1;">
	<div data-delay="5000" class="toast m-2 shadow" role="alert" id="element" aria-live="assertive" aria-atomic="true">
		<div class="alert alert-success mb-0" role="alert">
	 		Аирдроп добавлен в список выполненных
		</div>
	</div>
	<div data-delay="5000" class="toast m-2 shadow" role="alert" id="element1" aria-live="assertive" aria-atomic="true">
		<div class="alert alert-danger mb-0" role="alert">
	 		Аирдроп удален из списка выполненных
		</div>
	</div>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script>
// 	$('#switch').click(function() {
//   $.ajax({
//     type: 'POST',
//     url: '/ajax',
//     data: $(this).serialize(),
//     success: function(response) {
//       const jsonData = JSON.parse(response);
//       if (jsonData.success == "1") {
//         alert('Success');
//       } else {
//         alert('Ничего не происходит');
//       }
//     }
//   });
// });
// if ($('#switch').is(':checked')){
//     alert('Включен');
// } else {
//     alert('Выключен');
// }

// добавление аирдропу к юзеру
$('#switch').click(function() {
	id = $(this).attr('data-id');
	if ($(this).is(':checked')) {
		status = 1;
	} else {
		status = 0;
	}
	var data = {id : id, status : status};

	$.ajax({
		type: 'POST',
		url: '/ajax',
		data: data,
		success: function(response) {
			if (!response) {
				$('#exampleModal').modal('show');
				$("#switch").each(function() { this.checked=false; });
			}
			if (response) {
				if (data.status == 1) {
					$('#element').parent().css("z-index", 1);
					$('#element').toast('show');
					setTimeout("$('#element').parent().css('z-index', 1);", 5001);
				}
				if (data.status == 0) {
					$('#element1').parent().css("z-index", 1);
					$('#element1').toast('show');
					setTimeout("$('#element1').parent().css('z-index', 1);", 5001);
				}
				const jsonData = JSON.stringify(response);
				console.log(jsonData);
				//console.log(data);
			}
		}
	});
});


// добавление заметки к аирдропу
$('.addNote').click(function() {
	user = $(this).attr('data-user');
	airdrop = $(this).attr('data-airdrop');
	note = $('#note_' + airdrop).val();
	data = {user_id : user, airdrop_id : airdrop, note : note};
	//console.log(data);
	$.ajax({
		type: 'POST',
		url: '/addNote',
		data: data,
		success: function(response){
			//console.log(data);
			if (data.note === "") {
				$('#badge_' + data.airdrop_id).addClass('d-none');
			}
			if (data.note !== "") {
				$('#badge_' + data.airdrop_id).removeClass('d-none').html('З');
			}
			
		}
	});
});

// Отметка выплачен/невыплачен
$('.delete').click(function(e) {
	e.preventDefault();
	id = $(this).attr('data-id');
	user = $('#paid-' + id).attr('data-user');
	airdrop = $('#paid-' + id).attr('data-airdrop');
	status = 1;
	data = {user_id : user, airdrop_id : airdrop, status : status};
	$.ajax({
		type: 'POST',
		url: '/status',
		data: data
	});
	$(this).addClass('d-none');
	$('#add-delete-' + id).removeClass('d-none');
	$('#paid-' + id).removeClass('d-none');
});

$('.add-delete').click(function(e) {
	e.preventDefault();
	id = $(this).attr('data-id');
	user = $('#paid-' + id).attr('data-user');
	airdrop = $('#paid-' + id).attr('data-airdrop');
	status = 0;
	data = {user_id : user, airdrop_id : airdrop, status : status};
	$.ajax({
		type: 'POST',
		url: '/status',
		data: data
	});
	$(this).addClass('d-none');
	$('#delete-' + id).removeClass('d-none');
	$('#paid-' + id).addClass('d-none');
});

$('.delDrop').click(function(e){
	e.preventDefault();
	id = $(this).attr('data-id');
	user = $('#paid-' + id).attr('data-user');
	airdrop = $('#paid-' + id).attr('data-airdrop');
	data = {user_id : user, airdrop_id : airdrop};
	$.ajax({
		type: 'POST',
		url: '/delDrop',
		data: data,
		success: function(response) {
			$('#air_' + id).hide();
		}
	});
});

// $(document).on('click', '.fa-edit', function(){
// 	var id = $(this).attr('id');
// 	$.ajax({
// 		url:'/getdrop',
// 		method: 'POST',
// 		data: {id:id},
// 		dataType: 'json',
// 		success: function(data){
// 			$('#name').val(data[0].name);
// 			$('#website').val(data[0].website);
// 			$('#aliace').val(data[0].aliace);
// 			$('#ticker').val(data[0].ticker);
// 			$('#start_link').val(data[0].start_link);
// 			$('#num_tokens').val(data[0].num_tokens);
// 			$('#fiat_price').val(data[0].fiat_price);
// 			$('#descr').val(data[0].descr);
// 			//$('#logo').val(data[0].logo);
// 			$.each(data[1], function(i, val){
// 				$('input:checkbox[name="req_tools[]"]').filter('[value=' + val.id + ']').attr('checked', true);
// 			});
// 			var date = new Date(data[0].expire_date*1000);
// 			var year = date.getFullYear();
// 			var day = ("0" + date.getDate()).slice(-2);
// 			var month = ("0" + (date.getMonth() + 1)).slice(-2);
// 			var newDate = year + '-' + month + '-' + day;
// 			$('#date').val(newDate);
// 			$('input:radio[name="is_ref"]').filter('[value=' + data[0].is_ref + ']').attr('checked', true);
// 			$('#instruction').val(data[0].instruction);
// 			$('input:radio[name="is_done"]').filter('[value=' + data[0].is_done + ']').attr('checked', true);
// 			$('input:radio[name="is_rec"]').filter('[value=' + data[0].is_rec + ']').attr('checked', true);
// 			$('input:radio[name="is_hot"]').filter('[value=' + data[0].is_hot + ']').attr('checked', true);
// 			$('input:radio[name="is_pin"]').filter('[value=' + data[0].is_pin + ']').attr('checked', true);
// 			$('input:radio[name="is_new"]').filter('[value=' + data[0].is_new + ']').attr('checked', true);
// 			$('#btn').val('Обновить');
// 			$('#addAirMoadl').modal('show');
// 		}
// 	});
// });

// $('#addAirMoadl').on('hidden.bs.modal', function () {
//     $(this).find('form').trigger('reset');
    
// })

    
</script>
</body>
</html>