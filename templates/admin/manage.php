<?php include '../templates/layout/header.php'; ?>
<div class="container">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb bg-light shadow-sm mb-0 border mt-3">
	    <li class="breadcrumb-item"><a href="/admin">Админ</a></li>
	    <li class="breadcrumb-item acrive" aria-current="page">Управление</li>
	  </ol>
	</nav>
</div>
	<div class="container">
		<h2>Управление аирдропами</h2>

			<div class="row mb-3">
				<div class="col-12 col-lg-3">
					<?php if (isset($errors) && is_array($errors)): ?>
				        <div class="alert alert-danger" role="alert">
				            <?php foreach ($errors as $error): ?>
				                -<?php echo $error; ?><br>
				            <?php endforeach;?>
				        </div>
				    <?php endif; ?>
					<a class="btn btn-primary" data-toggle="modal" data-target="#addAirMoadl" href="#" role="button"><i class="fas fa-plus"></i> Добавить аирдроп</a>
				</div>
			</div>
			<div class="table-responsive">
			<table class="table bg-light table-striped table-hover">
    <thead>
      <tr>
        <th>Название</th>
        <th>Тикер</th>
        <th>Количество монет</th>
        <th>Количество денег</th>
        <th>Ссылка на проект</th>
        <th>Действия</th>
      </tr>
    </thead>
    <tbody>
      <?php if($airdrops): foreach($airdrops as $airdrop): ?>

	      <tr class="">
	        <td><?=$airdrop['name']?></td>
	        <td><?=$airdrop['ticker']?></td>
	        <td><?=$airdrop['num_tokens']?></td>
	        <td><?=$airdrop['fiat_price']?></td>
	        <td><a target="_blank" href="http://<?=$airdrop['website']?>"><?=$airdrop['website']?></a></td>
	        <td>
	        	<a title="Редактировать" href="/edit?id=<?=$airdrop['id']?>"><i class="fas fa-lg fa-edit" id="<?=$airdrop['id']?>"></i></i></a>
                <a title="Удалить" href="/delete?id=<?=$airdrop['id']?>"><i class="fas fa-lg fa-times text-danger"></i></a>
	        </td>
	      </tr>

      <?php endforeach; endif; ?>
      
    </tbody>
  </table>
</div>
			

			<div class="modal fade" id="addAirMoadl" tabindex="-1" role="dialog" aria-labelledby="addAirMoadl" aria-hidden="true">
			  <div class="modal-dialog modal-dialog-centered" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalCenterTitle">Добавление аирдропа</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			      	<div class="container-fluid">
			      		<form action="" method="post" id="addAir" enctype="multipart/form-data">
			      			<div class="form-row">
			      				<div class="col-12">
			      					<h6>СЕО</h6>
			      				</div>
			      			</div>
		      				<div class="form-row">
			      				<div class="form-group col-6">
			      					<label for=""><h6>Заголовок (title)</h6></label>
      								<input type="text" name="title" class="form-control" placeholder="Title" id="title">
			      				</div>
			      				<div class="form-group col-6">
			      					<label for=""><h6>Keywords</h6></label>
      								<input type="text" name="meta_k" class="form-control" placeholder="Keywords" id="title">
			      				</div>
			      			</div>
			      			<div class="form-row">
			      				<div class="form-group col-6">
			      					<label for=""><h6>Meta desc</h6></label>
      								<input type="text" name="meta_d" class="form-control" placeholder="Meta desc" id="title">
			      				</div>
			      				<div class="form-group col-6">
			      					<label for=""><h6>H1</h6></label>
      								<input type="text" name="h1" class="form-control" placeholder="H1" id="title">
			      				</div>
			      			</div>
			      			<div class="form-row">
			      				<div class="form-group col-6">
			      					<label for=""><h6>H2</h6></label>
      								<input type="text" name="h2" class="form-control" placeholder="H2" id="title">
			      				</div>
			      			</div>
			      			<div class="form-row">
			      				<div class="form-group col-md-6">
			      					<label for=""><h6>Название аирдропа</h6></label>
      								<input type="text" name="name" class="form-control" placeholder="Название" id="name" required="">
			      				</div>
			      				<div class="form-group col-md-6">
			      					<label for=""><h6>Вебсайт аирдропа</h6></label>
      								<input type="text" name="website" id="website" class="form-control" placeholder="Вебсайт">
			      				</div>
			      			</div>
			      			<div class="form-row">
			      				<div class="form-group col-md-6">
			      					<label for=""><h6>Ссылка на аирдроп (алиас)</h6></label>
      								<input type="text" name="aliace" id="aliace" class="form-control" placeholder="Ссылка">
			      				</div>
			      				<div class="form-group col-md-6">
			      					<label for=""><h6>Название токена (ticker)</h6></label>
      								<input type="text" name="ticker" id="ticker" class="form-control" placeholder="BTC, RIPL, ETH...">
			      				</div>
			      			</div>
			      			<div class="form-row">
			      				<div class="form-group col-md-6">
			      					<label for=""><h6>Ссылка "START"</h6></label>
      								<input type="text" name="start_link" id="start_link" class="form-control" placeholder="Ссылка">
			      				</div>
			      				<div class="form-group col-md-6">
			      					<label for=""><h6>Ссылка Видео инструкция</h6></label>
      								<input type="text" name="youtube_link" id="youtube_link" class="form-control" placeholder="https://">
			      				</div>
			      			</div>
			      			<div class="form-row">
			      				<div class="form-group col-md-12">
			      					<label for=""><h6>Кол-во токенов на человека</h6></label>
      								<input type="number" name="num_tokens" id="num_tokens" class="form-control" placeholder="">
			      				</div>
			      			</div>
			      			<div class="form-row">
			      				<div class="form-group col-md-12">
			      					<label for=""><h6>Вознаграждение в $ на человека</h6></label>
      								<input type="text" name="fiat_price" id="fiat_price" class="form-control" placeholder="">
			      				</div>
			      			</div>
			      			<div class="form-row">
			      				<div class="form-group col-md-12">
			      					<label for=""><h6>Описание аирдропа</h6></label>
			      					<textarea name="descr" id="descr" class="form-control"></textarea>
			      				</div>
			      			</div>
			      			<div class="form-group">
							    <label for="exampleFormControlFile1"><h6>Лого аирдропа</h6></label>
							    <input type="file" name="logo" id="logo" class="form-control-file" id="exampleFormControlFile1">
							 </div>
							 
							 <div class="form-row">
							 	<div class="col-md-12">
							 		<h6>Необходимые инструменты</h6>
							 	</div>
								
								<?php if($tools): foreach($tools as $tool): ?>

								 	<div class="form-group col-md-3">
										 <div class="form-check">
											<input class="form-check-input" name="req_tools[]" type="checkbox" value="<?=$tool['id']?>">
											<label class="form-check-label"><?=$tool['name']?></label>
										 </div>
									 </div>

								<?php endforeach; endif; ?>

							 </div>
							 <div class="form-row">
			      				<div class="form-group col-md-6">
			      					<label for=""><h6>Дата окончания аирдропа</h6></label>
      								<input type="date" class="form-control" name="date" id="date" placeholder="Дата">
			      				</div>
			      				<!-- <div class="form-group col-md-6">
			      					<label for=""><h6>Ваше имя</h6></label>
      								<input type="text" class="form-control" placeholder="Имя">
			      				</div> -->
			      			<!-- </div> -->
			      			<!-- <div class="form-row">
			      				<div class="form-group col-md-6">
			      					<label for=""><h6>Ваш email</h6></label>
      								<input type="email" class="form-control" id="" placeholder="Email">
			      				</div>
			      			</div> -->
			      			<div class="form-row">
			      				<div class="col-md-12">
							 		<h6>Реферальная программа:</h6>
							 	</div>
			      				<div class="form-group col-md-12">
									 <div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="is_ref" id="" value="1">
										<label class="form-check-label" for="inlineCheckbox1">Да</label>
									 </div>
									 <div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="is_ref" id="" value="0" checked>
										<label class="form-check-label" for="inlineCheckbox1">Нет</label>
									 </div>
								 </div>
			      			</div>
			      			<div class="form-row">
			      				<div class="form-group col-md-12">
			      					<label for=""><h6>Инструкция (каждый этап с новой строки):</h6></label>
			      					<textarea name="instruction" id="instruction" class="form-control"></textarea>
			      				</div>
			      			</div>
			      			<div class="form-row">
			      				<div class="col-md-12">
							 		<h6>Выполнен:</h6>
							 	</div>
			      				<div class="form-group col-md-12">
									 <div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="is_done" id="is_ref" value="1">
										<label class="form-check-label" for="inlineCheckbox1">Да</label>
									 </div>
									 <div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="is_done" id="is_refno" value="0" checked>
										<label class="form-check-label" for="inlineCheckbox1">Нет</label>
									 </div>
								 </div>
			      			</div>
			      			<div class="form-row">
			      				<div class="col-md-12">
							 		<h6>Рекомендованный (сайдбар):</h6>
							 	</div>
			      				<div class="form-group col-md-12">
									 <div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="is_rec" id="is_ref" value="1">
										<label class="form-check-label" for="inlineCheckbox1">Да</label>
									 </div>
									 <div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="is_rec" id="is_refno" value="0" checked>
										<label class="form-check-label" for="inlineCheckbox1">Нет</label>
									 </div>
								 </div>
			      			</div>
			      			<div class="form-row">
			      				<div class="col-md-12">
							 		<h6>Горячий:</h6>
							 	</div>
			      				<div class="form-group col-md-12">
									 <div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="is_hot" id="is_ref" value="1">
										<label class="form-check-label" for="inlineCheckbox1">Да</label>
									 </div>
									 <div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="is_hot" id="is_refno" value="0" checked>
										<label class="form-check-label" for="inlineCheckbox1">Нет</label>
									 </div>
								 </div>
			      			</div>
			      			<div class="form-row">
			      				<div class="col-md-12">
							 		<h6>Закрепленный:</h6>
							 	</div>
			      				<div class="form-group col-md-12">
									 <div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="is_pin" id="is_pin" value="1">
										<label class="form-check-label" for="inlineCheckbox1">Да</label>
									 </div>
									 <div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="is_pin" id="is_pin" value="0" checked>
										<label class="form-check-label" for="inlineCheckbox1">Нет</label>
									 </div>
								 </div>
			      			</div>
			      			<div class="form-row">
			      				<div class="col-md-12">
							 		<h6>Новый:</h6>
							 	</div>
			      				<div class="form-group col-md-12">
									 <div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="is_new" id="is_new" value="1">
										<label class="form-check-label" for="inlineCheckbox1">Да</label>
									 </div>
									 <div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="is_new" id="is_new" value="0" checked>
										<label class="form-check-label" for="inlineCheckbox1">Нет</label>
									 </div>
								 </div>
			      			</div>
			      		</form>
			      	</div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
			        <input type="submit" name="submit" class="btn btn-primary" form="addAir" id="btn" value="Сохранить">
			      </div>
			    </div>
			  </div>
			</div>

			<div class="modal fade" id="deleteAirModal" tabindex="-1" role="dialog" aria-labelledby="deleteAirModal" aria-hidden="true">
				
			</div>

	</div>
<div class="container">
	<div class="row">
		<nav aria-label="Page navigation">
			<ul class="pagination">
				<?php echo ($airdrops ? $pagination : 'Аирдропов не найдено!'); ?>
			</ul>
		</nav>
	</div>
</div>
<?php include '../templates/layout/footer.php'; ?>