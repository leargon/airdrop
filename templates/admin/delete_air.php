<?php include '../templates/layout/header.php'; ?>
<div class="container">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb bg-light shadow-sm mb-0 border mt-3">
	    <li class="breadcrumb-item"><a href="/admin">Админ</a></li>
	    <li class="breadcrumb-item"><a href="/admin/manage">Управление</a></li>
	    <li class="breadcrumb-item acrive" aria-current="page">Удаление</li>
	  </ol>
	</nav>
</div>
	<div class="container">
		<h2>Вы действительно хотите удалить аирдроп №<?=$_GET['id']?>?</h2>
		<form method="post">
		<input class="btn btn-danger" type="submit" name="submit" value="Удалить">
		<a class="btn btn-success" href="javascript: history.back(-1);">Нет</a>
		</form>
	</div>
	
<?php include '../templates/layout/footer.php'; ?>