<?php include '../templates/layout/header.php'; ?>
<div class="container">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb bg-light shadow-sm mb-0 border mt-3">
	    <li class="breadcrumb-item"><a href="/admin">Админ</a></li>
	    <li class="breadcrumb-item"><a href="/admin/manage">Управление</a></li>
	    <li class="breadcrumb-item acrive" aria-current="page">Редактирование</li>
	  </ol>
	</nav>
</div>
	<div class="container">
		<h2>Редактировать аирдроп</h2>
				<div class="container bg-white mb-2">
			      		<form action="#" method="post" id="addAir" enctype="multipart/form-data">
			      			<div class="form-row">
			      				<div class="col-12">
			      					<h6>СЕО</h6>
			      				</div>
			      			</div>
		      				<div class="form-row">
			      				<div class="form-group col-6">
			      					<label for=""><h6>Заголовок (title)</h6></label>
      								<input type="text" name="title" class="form-control" placeholder="Title" id="title" value="<?=$airdrop[0]['title']?>">
			      				</div>
			      				<div class="form-group col-6">
			      					<label for=""><h6>Keywords</h6></label>
      								<input type="text" name="meta_k" class="form-control" placeholder="Keywords" id="title" value="<?=$airdrop[0]['meta_k']?>">
			      				</div>
			      			</div>
			      			<div class="form-row">
			      				<div class="form-group col-6">
			      					<label for=""><h6>Meta desc</h6></label>
      								<input type="text" name="meta_d" class="form-control" placeholder="Meta desc" id="title" value="<?=$airdrop[0]['meta_d']?>">
			      				</div>
			      				<div class="form-group col-6">
			      					<label for=""><h6>H1</h6></label>
      								<input type="text" name="h1" class="form-control" placeholder="H1" id="title" value="<?=$airdrop[0]['h1']?>">
			      				</div>
			      			</div>
			      			<div class="form-row">
			      				<div class="form-group col-6">
			      					<label for=""><h6>H2</h6></label>
      								<input type="text" name="h2" class="form-control" placeholder="H2" id="title" value="<?=$airdrop[0]['h2']?>">
			      				</div>
			      			</div>
			      			<div class="form-row">
			      				<div class="form-group col-md-6">
			      					<label for=""><h6>Название аирдропа</h6></label>
      								<input type="text" name="name" class="form-control" placeholder="Название" id="name" required="" value="<?=$airdrop[0]['name']?>">
			      				</div>
			      				<div class="form-group col-md-6">
			      					<label for=""><h6>Вебсайт аирдропа</h6></label>
      								<input type="text" name="website" id="website" class="form-control" placeholder="Вебсайт" value="<?=$airdrop[0]['website']?>">
			      				</div>
			      			</div>
			      			<div class="form-row">
			      				<div class="form-group col-md-6">
			      					<label for=""><h6>Ссылка на аирдроп (алиас)</h6></label>
      								<input type="text" name="aliace" id="aliace" class="form-control" placeholder="Ссылка" value="<?=$airdrop[0]['aliace']?>">
			      				</div>
			      				<div class="form-group col-md-6">
			      					<label for=""><h6>Название токена (ticker)</h6></label>
      								<input type="text" name="ticker" id="ticker" class="form-control" placeholder="BTC, RIPL, ETH..." value="<?=$airdrop[0]['ticker']?>">
			      				</div>
			      			</div>
			      			<div class="form-row">
			      				<div class="form-group col-md-6">
			      					<label for=""><h6>Ссылка "START"</h6></label>
      								<input type="text" name="start_link" id="start_link" class="form-control" placeholder="Ссылка" value="<?=$airdrop[0]['start_link']?>">
			      				</div>
			      				<div class="form-group col-md-6">
			      					<label for=""><h6>Ссылка Видео инструкция</h6></label>
      								<input type="text" name="youtube_link" id="youtube_link" class="form-control" placeholder="https://" value="<?=$airdrop[0]['youtube_link']?>">
			      				</div>
			      			</div>
			      			<div class="form-row">
			      				<div class="form-group col-md-6">
			      					<label for=""><h6>Кол-во токенов на человека</h6></label>
      								<input type="text" name="num_tokens" id="num_tokens" class="form-control" placeholder="" value="<?=$airdrop[0]['num_tokens']?>">
			      				</div>
			      				<div class="form-group col-md-6">
			      					<label for=""><h6>Вознаграждение в $ на человека</h6></label>
      								<input type="text" name="fiat_price" id="fiat_price" class="form-control" placeholder="" value="<?=$airdrop[0]['fiat_price']?>">
			      				</div>
			      			</div>
			      			<div class="form-row">
			      				<div class="form-group col-md-12">
			      					<label for=""><h6>Описание аирдропа</h6></label>
			      					<textarea name="descr" id="descr" class="form-control"><?=$airdrop[0]['descr']?></textarea>
			      				</div>
			      			</div>
			      			<div class="form-group">
							    <label for="exampleFormControlFile1"><h6>Лого аирдропа</h6></label>
							    <img src="<?=getImage($airdrop[0]['name']);?>" width="50" alt="" />
							    <input type="file" name="logo" id="logo" class="form-control-file" id="exampleFormControlFile1">
							 </div>
							 
							 <div class="form-row">
							 	<div class="col-md-12">
							 		<h6>Необходимые инструменты</h6>
							 	</div>

								<?php if($tools): foreach($tools as $tool): ?>

								 	<div class="form-group col-md-3">
										 <div class="form-check">
											<input class="form-check-input" name="req_tools[]" type="checkbox" value="<?=$tool['id']?>" 
											
											<?php if ($tool_rel): foreach($tool_rel as $value): if($value['id'] == $tool['id']): ?>
												checked
											<?php endif; endforeach; endif; ?>
												>
											<label class="form-check-label"><?=$tool['name']?></label>
										 </div>
									 </div>

								<?php endforeach; endif; ?>

							 </div>
							 <div class="form-row">
			      				<div class="form-group col-md-6">
			      					<label for=""><h6>Дата окончания аирдропа</h6></label>
      								<input type="date" class="form-control" name="date" id="date" placeholder="Дата" value="<?=date('Y-m-d', $airdrop[0]['expire_date'])?>">
			      				</div>
			      				<!-- <div class="form-group col-md-6">
			      					<label for=""><h6>Ваше имя</h6></label>
      								<input type="text" class="form-control" placeholder="Имя">
			      				</div> -->
			      			<!-- </div> -->
			      			<!-- <div class="form-row">
			      				<div class="form-group col-md-6">
			      					<label for=""><h6>Ваш email</h6></label>
      								<input type="email" class="form-control" id="" placeholder="Email">
			      				</div> -->
			      			</div>
			      			<div class="form-row">
			      				<div class="col-md-12">
							 		<h6>Реферальная программа:</h6>
							 	</div>
			      				<div class="form-group col-md-12">
									 <div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="is_ref" id="is_ref" value="1" <?php if($airdrop[0]['is_ref'] == 1) echo 'checked'?>>
										<label class="form-check-label" for="inlineCheckbox1">Да</label>
									 </div>
									 <div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="is_ref" id="is_refno" value="0" <?php if($airdrop[0]['is_ref'] == 0) echo 'checked'?>>
										<label class="form-check-label" for="inlineCheckbox1">Нет</label>
									 </div>
								 </div>
			      			</div>
			      			<div class="form-row">
			      				<div class="form-group col-md-12">
			      					<label for=""><h6>Инструкция (каждый этап с новой строки):</h6></label>
			      					<textarea name="instruction" id="instruction" class="form-control"><?=$airdrop[0]['instruction']?></textarea>
			      				</div>
			      			</div>
			      			<div class="form-row">
			      				<div class="col-md-12">
							 		<h6>Выполнен:</h6>
							 	</div>
			      				<div class="form-group col-md-12">
									 <div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="is_done" id="is_ref" value="1"<?php if($airdrop[0]['is_done'] == 1) echo 'checked'?>>
										<label class="form-check-label" for="inlineCheckbox1">Да</label>
									 </div>
									 <div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="is_done" id="is_refno" value="0"<?php if($airdrop[0]['is_done'] == 0) echo 'checked'?>>
										<label class="form-check-label" for="inlineCheckbox1">Нет</label>
									 </div>
								 </div>
			      			</div>
			      			<div class="form-row">
			      				<div class="col-md-12">
							 		<h6>Рекомендованный (сайдбар):</h6>
							 	</div>
			      				<div class="form-group col-md-12">
									 <div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="is_rec" id="is_ref" value="1"<?php if($airdrop[0]['is_rec'] == 1) echo 'checked'?>>
										<label class="form-check-label" for="inlineCheckbox1">Да</label>
									 </div>
									 <div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="is_rec" id="is_refno" value="0"<?php if($airdrop[0]['is_rec'] == 0) echo 'checked'?>>
										<label class="form-check-label" for="inlineCheckbox1">Нет</label>
									 </div>
								 </div>
			      			</div>
			      			<div class="form-row">
			      				<div class="col-md-12">
							 		<h6>Горячий:</h6>
							 	</div>
			      				<div class="form-group col-md-12">
									 <div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="is_hot" id="is_ref" value="1"<?php if($airdrop[0]['is_hot'] == 1) echo 'checked'?>>
										<label class="form-check-label" for="inlineCheckbox1">Да</label>
									 </div>
									 <div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="is_hot" id="is_refno" value="0"<?php if($airdrop[0]['is_hot'] == 0) echo 'checked'?>>
										<label class="form-check-label" for="inlineCheckbox1">Нет</label>
									 </div>
								 </div>
			      			</div>
			      			<div class="form-row">
			      				<div class="col-md-12">
							 		<h6>Закрепленный:</h6>
							 	</div>
			      				<div class="form-group col-md-12">
									 <div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="is_pin" id="is_pin" value="1"<?php if($airdrop[0]['is_pin'] == 1) echo 'checked'?>>
										<label class="form-check-label" for="inlineCheckbox1">Да</label>
									 </div>
									 <div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="is_pin" id="is_pin" value="0"<?php if($airdrop[0]['is_pin'] == 0) echo 'checked'?>>
										<label class="form-check-label" for="inlineCheckbox1">Нет</label>
									 </div>
								 </div>
			      			</div>
			      			<div class="form-row">
			      				<div class="col-md-12">
							 		<h6>Новый:</h6>
							 	</div>
			      				<div class="form-group col-md-12">
									 <div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="is_new" id="is_new" value="1"<?php if($airdrop[0]['is_new'] == 1) echo 'checked'?>>
										<label class="form-check-label" for="inlineCheckbox1">Да</label>
									 </div>
									 <div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="is_new" id="is_new" value="0"<?php if($airdrop[0]['is_new'] == 0) echo 'checked'?>>
										<label class="form-check-label" for="inlineCheckbox1">Нет</label>
									 </div>
								 </div>
			      			</div>
			      			<div class="form-row">
			      				<div class="form-group col-md-12">
			      					<input type="submit" name="submit" class="btn btn-primary" form="addAir" id="btn" value="Сохранить">
			      				</div>
			      			</div>
			      		</form>
			      	</div>
			      
			      
			   

			

	</div>

<?php include '../templates/layout/footer.php'; ?>