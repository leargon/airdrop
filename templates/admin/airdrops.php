<?php include '../templates/layout/header.php'; ?>
	<div class="container">
		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb bg-light shadow-sm mb-0 border mt-3">
		    <li class="breadcrumb-item"><a href="/admin">Админ</a></li>
		    <li class="breadcrumb-item acrive" aria-current="page">Аирдропы</li>
		  </ol>
		</nav>
	</div>
	<div class="container">
		<h2>Мои аирдропы</h2>

			<?php if($airs): foreach($airs as $airdrop): ?>

				<div class="alert alert-success shadow row align-items-center airdrop" id="air_<?=$airdrop['id']?>">
					<div class="col-3 col-lg-1"><img src="<?=getImage($airdrop['name']);?>" alt="" class="img-fluid"></div>
					<div class="col-9 col-lg-4"><h4 class="alert-heading text-center"><?=$airdrop['name']?> (<?=$airdrop['ticker']?>)</h4></div>
					<div class="col-12 col-lg-3"><h6 class="text-center"><i class="fas fa-coins"></i> <?=$airdrop['num_tokens']?> <small>токенов</small> <i class="far fa-money-bill-alt" style="color: #00F900"></i> <?=$airdrop['fiat_price']?>$</h6></div>
					<div class="col-12 col-lg-3 text-center">
						<i class="fas fa-lg fa-money-check-alt <?php if(getStatusByRelUserAir($_SESSION['user'], $airdrop['id']) == 0) echo 'd-none'; ?>" data-airdrop="<?=$airdrop['id']?>" data-user="<?=$_SESSION['user']?>" data-status="<?=getStatusByRelUserAir($_SESSION['user'], $airdrop['id'])?>" id="paid-<?=$airdrop['id']?>" title="Выплачен" data-toggle="tooltip"></i>
						<div class="dropdown d-inline" data-placement="top" title="Информация" data-toggle="tooltip">
							<button class="btn btn-secondary dropdown-toggle" href="#collapseIns<?=$airdrop['id']?>" type="button" data-toggle="collapse" aria-haspopup="true" aria-expanded="true">
						    <strong>i</strong>
						    </button>
						    <div class="dropdown-menu dropdown-menu-bot" aria-labelledby="dropdownMenuButton">
								<p class="p-2"><?=$airdrop['descr']?></p>
							</div>
						</div>
						<div class="dropdown d-inline" data-placement="top" title="Заметки" data-toggle="tooltip">
							<button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="collapse" data-target="#collapseIns<?=$airdrop['id']?>2" aria-expanded="false" aria-controls="multiCollapseExample2"><strong><i class="far fa-folder-open"></i>

								<span id="badge_<?=$airdrop['id']?>" class="badge badge-primary<?php if(!getNotesByRelUserAir($_SESSION['user'], $airdrop['id'])) echo ' d-none'; ?>"><?php if(getNotesByRelUserAir($_SESSION['user'], $airdrop['id'])) echo 'З'; ?></span>

							</strong></button>
						    
						    </button>
						</div>
						<div class="dropdown d-inline">
							<button class="btn btn-secondary" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
								<i class="fas fa-caret-down"></i>
						    </button>
						    <div class="dropdown-menu dropdown-menu-bot" aria-labelledby="dropdownMenuButton">
								<a class="dropdown-item delete <?php if(getStatusByRelUserAir($_SESSION['user'], $airdrop['id']) == 1) echo 'd-none'; ?>" id="delete-<?=$airdrop['id']?>" href="#" data-id="<?=$airdrop['id']?>">Отметить как выплаченный</a>
								<a class="dropdown-item <?php if(getStatusByRelUserAir($_SESSION['user'], $airdrop['id']) == 0) echo 'd-none'; ?> add-delete" href="#" id="add-delete-<?=$airdrop['id']?>" data-id="<?=$airdrop['id']?>">Снять отметку как выплаченный</a>
								<a class="dropdown-item delDrop" href="#" data-id="<?=$airdrop['id']?>">Удалить</a>
							</div>
						</div>
					</div>
					<div class="collapse multi-collapse mt-2 col-12" id="collapseIns<?=$airdrop['id']?>">
					  <div class="card card-body">
					    <?=textToLi($airdrop['instruction'])?>
					  </div>
					</div>
					<div class="collapse multi-collapse mt-2 col-12" id="collapseIns<?=$airdrop['id']?>2">
					  <div class="card card-body">
					   <textarea class="form-control" name="" id="note_<?=$airdrop['id']?>" cols="100%" rows="10"><?=getNotesByRelUserAir($_SESSION['user'], $airdrop['id'])?></textarea>
					   <input class="btn btn-primary addNote" type="button" value="Сохранить" data-user="<?=$_SESSION['user']?>" data-airdrop="<?=$airdrop['id']?>">
					  </div>
					</div>
				</div>

			<?php endforeach; endif; ?>

	</div>
<?php include '../templates/layout/footer.php'; ?>