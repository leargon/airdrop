<?php 
//var_dump($login);
include '../templates/layout/header.php'; ?>
<div class="container p-3">
	<div class="row">
		<div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
	<div class="card p-3 shadow">
	<h1>Войти</h1>
	 <?php if (isset($errors) && is_array($errors)): ?>
        <div class="alert alert-danger" role="alert">
            <?php foreach ($errors as $error): ?>
                -<?php echo $error; ?><br>
            <?php endforeach;?>
        </div>
    <?php endif; ?>
	<form action="" method="post">
        <div class="form-group ">
	      <label for="inputEmail4">Ваш email:</label>
	      <input type="email" name="name" class="form-control" placeholder="login" required="">
	    </div>
	    <div class="form-group">
	      <label for="inputEmail4">Пароль:</label>
	      <input type="password" name="pass" class="form-control" placeholder="password" required="">
	    </div>
	    <div class="form-group">
	    	<!-- <input type="submit" name="submit" class="btn btn-primary" value="ВОЙТИ"> -->
	    	<input class="btn btn-lg btn-primary btn-block text-uppercase" name="submit" type="submit" value="ВОЙТИ">
	    </div>
    </form>
    <p class="text-center">или<br><a href="/register">Зарегистрируйтесь</a></p>
    </div>
    </div>
    </div>
</div>
<?php include '../templates/layout/footer.php'; ?>