<?php include '../templates/layout/header.php';?>
<div class="container p-3">
	<div class="row">
		<div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
	<div class="card p-3 shadow">
	<h1>Регистрация</h1>
	 <?php if (isset($errors) && is_array($errors)): ?>
        <div class="alert alert-danger" role="alert">
            <?php foreach ($errors as $error): ?>
                -<?php echo $error."<br>"; ?>
            <?php endforeach;?>
        </div>
    <?php endif; ?>
	<?php if(isset($success)): ?>
		<div class="alert alert-success" role="alert">
		  <?=$success?>
		</div>
		<h5 class="text-center">Можете войти со своими данными</h5>
		<p class="text-center"><a href="/login">Войти</a></p>
	<?php endif; ?>
    <?php if (!isset($success)): ?>
	<form action="" method="post">
        <div class="form-group ">
	      <label for="inputEmail4">Ваш email:</label>
	      <input type="email" name="name" class="form-control" placeholder="login" required="">
	    </div>
	    <div class="form-group">
	      <label for="inputEmail4">Пароль:</label>
	      <input type="password" name="pass" class="form-control" placeholder="password" required="">
	    </div>
	    <div class="form-group">
	      <label for="inputEmail4">Повторите пароль:</label>
	      <input type="password" name="passConf" class="form-control" placeholder="password" required="">
	    </div>
	    <!-- <div class="form-group">
	      <label for="inputEmail4">Повторите пароль:</label>
	      <input type="password" name="pass" class="form-control" placeholder="password" required="">
	    </div> -->
	    <div class="form-group">
	    	<!-- <input type="submit" name="submit" class="btn btn-primary" value="ВОЙТИ"> -->
	    	<input class="btn btn-lg btn-primary btn-block text-uppercase" name="submit" type="submit" value="РЕГИСТРАЦИЯ">
	    </div>
    </form>
    <p class="text-center">или<br><a href="/login">Войдите</a></p>
    <?php endif; ?>
    </div>
    </div>
    </div>
</div>
<?php include '../templates/layout/footer.php'; ?>