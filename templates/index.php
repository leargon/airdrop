<?php include 'layout/header.php'; ?>


	<!-- контент -->
	<div class="container mt-3">
		<div class="row">
			<?php include 'layout/sidebar.php'; ?>
			<div class="col-lg-8 text-center order-1 order-sm-2">

					<h1 class="text-left mb-3">Заработок на airdrop без вложений </h1>
					<h3 class="float-left h3">Список аирдропов</h3>
	
					<a class="text-dark h4"  data-toggle="collapse" href="#filter" role="button" aria-expanded="false" aria-controls="collapseExample"><i class="fas fa-filter"></i>Фильтры</a>
					<div class="float-right">
						<div class="dropdown">
							<span>Сорт. по</span>
							<a style="border-bottom: 1px black dotted;" class="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<?php echo textToSort(); ?>
							</a>
							<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<a class="dropdown-item" href="<?=sortUri('views')?>">Просмотры</a>
							<a class="dropdown-item" href="<?=sortUri('fiat_price')?>">Цена</a>
							<a class="dropdown-item" href="<?=sortUri('expire_date')?>">Дата окончания</a>
							<a class="dropdown-item" href="<?=sortUri('id')?>">Дата добавления</a>
							</div>
						</div>
						</div>
						<div class="clearfix"></div>
						<div class="collapse mb-3" id="filter">
							<div class="card card-body">
						    	<form action="#" method="GET">
						    		<h5 class="text-left">Требуемые инструменты</h5>
									<div class="form-row mb-3">

							    		<?php foreach($tools as $tool): ?>

											<div class="form-check form-check-inline col-3">
											  <input class="form-check-input" name="tool[]" type="checkbox" id="<?=$tool['name']?>" value="<?=$tool['id']?>" <?php if(!isset($_GET['filter'])) echo 'checked'; if(isset($_GET['tool'])) { foreach ($_GET['tool'] as $key => $value) {
											  	if ($value == $tool['id']) {
											  		echo "checked";
											  	} }
											  } ?>>
											  <label class="form-check-label" for="<?=$tool['name']?>"><?=$tool['name']?></label>
											</div>

										<?php endforeach; ?>
									</div>
									<button type="submit" name="filter" class="btn btn-primary">Обновить</button>
									<?php if(isset($_GET['search'])): ?>
										<input type="hidden" name="search" value="<?=$_GET['search']?>">
									<?php endif; ?>
						    	</form>
						    </div>  
						</div>
						
						<div class="clearfix"></div>
					
				

				<?php if ($airs): foreach ($airs as $key => $air): ?>

					<a href="/airdrop/<?=$air['aliace']?>" class="" style="color: black !important; text-decoration: none;">
						<div class="row shadow align-items-center mb-4 drop bg-white">
							<div class="col-lg-2 col-4">
								<img src="<?=getImage($air['name']);?>" alt="" class="square">
							</div>
							<div class="col-lg-3 col-4">
								<h5><?=$air['name']?></h5>
								<h6><i class="fas fa-coins"></i> <?=$air['num_tokens']?> <small>токенов</small> <i class="far fa-money-bill-alt" style="color: #00F900"></i> <?=$air['fiat_price']?>$</h6>
							</div>
							<div class="col-lg-2 text-center col-4">

								<?php if(getM2mToolsByAirdrop($air['id'])) : foreach (getM2mToolsByAirdrop($air['id']) as $key => $tool): ?>

							  	
							  	<i class="<?=$tool['icon']?>" data-toggle="tooltip" data-placement="top" title="<?=$tool['name']?> необходим для этой раздачи"></i>

							  <?php endforeach; endif; ?>
								<!-- <i class="fa-lg fab fa-telegram" data-toggle="tooltip" data-placement="top" title="Телеграм необходим для этой раздачи"></i>
								<i class="fa-lg fab fa-telegram"></i>
								<i class="fa-lg fab fa-telegram"></i>
								<i class="fa-lg fab fa-telegram"></i>
								<i class="fa-lg fab fa-telegram"></i> -->
							</div>
							<div class="col-lg-3 text-center col-6" data-toggle="tooltip" data-placement="top" title="Дата окончания">
								<i class="fas fa-hourglass-end"></i>
								<span>через 
								<?=expireAirdropTime($air['expire_date'])?></span>
							</div>
							<div class="col-lg-2 text-center col-6" data-toggle="tooltip" data-placement="top" title="Количество просмотров">
								<span><i class="fas fa-eye"></i> <?=$air['views']?></span>
							</div>
						</div>
					</a>

				<?php endforeach; endif; ?>
				
				<div class="row">
					<nav aria-label="Page navigation">
					  <ul class="pagination">
					    <?php echo ($airs ? $pagination : 'Аирдропов не найдено!'); ?>
					    <?php //echo $pagination->render(); ?>
					  </ul>
					</nav>
					<div class="col-lg-12">
						<h2 class="text-center mb-3">Бесплатная раздача криптовалюты airdrop</h2>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php include 'layout/footer.php'; ?>