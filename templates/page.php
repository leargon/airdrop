<?php include 'layout/header.php'; ?>

	<!-- контент -->
	<div class="container mt-3">
		<div class="row">
			<?php include 'layout/sidebar.php'; ?>
			<div class="col-lg-8 bg-white shadow mb-3 h-100 order-1 order-sm-2">
				<h1 class="h2"><?=$page[0]['name']?></h1>
				<div class="row">
					<div class="col">
						<p><?=$page[0]['descr']?></p>
					</div>
				</div>
				
		
			</div>
		</div>
	</div>

	<?php include 'layout/footer.php'; ?>