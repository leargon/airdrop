<?php include 'layout/header.php'; ?>


	<!-- контент -->
	<div class="container mt-3">
		<div class="row">
			<?php include 'layout/sidebar.php'; ?>
			<div class="col-lg-8">
				<div class="row">
					<h3>Список аирдропов</h3>
				</div>

				<?php if ($airs): foreach ($airs as $key => $air): ?>

					<a href="/airdrop/<?=$air['aliace']?>" class="" style="color: black !important; text-decoration: none;">
						<div class="row shadow align-items-center mb-4 drop bg-white">
							<div class="col-lg-2">
								<img src="img/air.png" alt="" class="img-fluid">
							</div>
							<div class="col-lg-3">
								<h5><?=$air['name']?></h5>
								<h6><i class="fas fa-coins"></i> <?=$air['num_tokens']?> <small>токенов</small> <i class="far fa-money-bill-alt" style="color: #00F900"></i> <?=$air['fiat_price']?>$</h6>
							</div>
							<div class="col-lg-2 text-center">

								<?php if(getM2mToolsByAirdrop($air['id'])) : foreach (getM2mToolsByAirdrop($air['id']) as $key => $tool): ?>

							  	
							  	<i class="<?=$tool['icon']?>" data-toggle="tooltip" data-placement="top" title="<?=$tool['name']?> необходим для этой раздачи"></i>

							  <?php endforeach; endif; ?>
								<!-- <i class="fa-lg fab fa-telegram" data-toggle="tooltip" data-placement="top" title="Телеграм необходим для этой раздачи"></i>
								<i class="fa-lg fab fa-telegram"></i>
								<i class="fa-lg fab fa-telegram"></i>
								<i class="fa-lg fab fa-telegram"></i>
								<i class="fa-lg fab fa-telegram"></i> -->
							</div>
							<div class="col-lg-3 text-center" data-toggle="tooltip" data-placement="top" title="Дата окончания">
								<i class="fas fa-hourglass-end"></i>
								<span>через <?=expireAirdropTime($air['expire_date'])?></span>
							</div>
							<div class="col-lg-2 text-center" data-toggle="tooltip" data-placement="top" title="Количество просмотров">
								<span><i class="fas fa-eye"></i> <?=$air['views']?></span>
							</div>
						</div>
					</a>

				<?php endforeach; endif; ?>
				
				<div class="row">
					<nav aria-label="Page navigation">
					  <ul class="pagination">
					    <?php echo ($airs ? $render : 'Аирдропов не найдено!'); ?>
					  </ul>
					</nav>
				</div>
				<!-- <ul class="pagination">
			        <li><a href="?pageno=1">Первая</a></li>
			        <li class="<?php if($pageno <= 1){ echo 'disabled'; } ?>">
			            <a href="<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=".($pageno - 1); } ?>">Назад</a>
			        </li>
			        <li><span><?=$pageno?></span></li>
			        <li class="<?php if($pageno >= $total_pages){ echo 'disabled'; } ?>">
			            <a href="<?php if($pageno >= $total_pages){ echo '#'; } else { echo "?pageno=".($pageno + 1); } ?>">Вперед</a>
			        </li>
			        <li><a href="?pageno=<?php echo $total_pages; ?>">Последняя</a></li>
			    </ul> -->

				

			</div>
		</div>
	</div>

	<?php include 'layout/footer.php'; ?>