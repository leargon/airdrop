<?php
// include '../app/db.php';
// include '../app/func.php';
// $airs = getAllAirdrops();

// foreach ($airs as $air) {
// 	echo $air['name'] . '. ' .
// 	'Количество токенов ' . $air['num_tokens'] . '.  ' .
// 	'Цена в фиате ' . $air['fiat_price'] . '.  ' .
// 	'Дата окончания ' . $air['expire_date'] . '.  ' .
// 	'Просмотров ' . $air['views'] . '.<br>'
// 	;
// }
//var_dump($airdrop[0]);
include 'layout/header.php';
?>


	<!-- контент -->
	<div class="container mt-3">
		<div class="row">
			<?php include 'layout/sidebar.php'; ?>
			<div class="col-lg-8 bg-white shadow mb-3 order-1 order-sm-2">
				<div class="row align-items-center mb-2">
					<div class="col-lg-2 col-3">
						<img src="<?=getImage($airdrop[0]['name'])?>" alt="" class="square">
					</div>
					<div class="col-lg-5 col-9 text-center">
						<h1 class="h2"><?=$airdrop[0]['name']?></h1>
						<h6><i class="fas fa-coins"></i> <?=$airdrop[0]['num_tokens']?> <small>токенов</small> <i class="far fa-money-bill-alt" style="color: #00F900"></i> <?=$airdrop[0]['fiat_price']?>$</h6>
					</div>
					<div class="col-lg-3 text-center col-6" data-toggle="tooltip" data-placement="top" title="Дата окончания">
						Заканчивается
						<span>через <?=expireAirdropTime($airdrop[0]['expire_date'])?></span>
					</div>
					<div class="col-lg-2 text-center col-6" data-toggle="tooltip" data-placement="top" title="Количество просмотров">
						<span><i class="fas fa-eye"></i> <?=$airdrop[0]['views']?> просмотра</span>
					</div>
					
				</div>
				<div class="row">
					<div class="col">
						<h3>Описание</h3>
						<p><?=$airdrop[0]['descr']?></p>
					</div>
				</div>

				<?php if($airdrop[0]['youtube_link']): ?>

					<div class="row mb-3">
						<div class="col">
							<h3>Видео инструкция</h3>
							<div class="embed-responsive embed-responsive-16by9">
							  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?=$airdrop[0]['youtube_link']?>" allowfullscreen></iframe>
							</div>
						</div>
					</div>

				<?php endif; ?>

				<div class="row mb-4">
					<div class="col">
						<h3>Инструкция</h3>
						<ul class="list-group list-group-flush">
							 
							 <?=textToLi($airdrop[0]['instruction'])?>
						  
						</ul>
					</div>
				</div>
				<div class="row mb-4" style="border-bottom:6px dotted #2eb9ce;">
					<div class="col">
						<h3>Требуемые инструменты</h3>

						  <?php if($tools) : foreach ($tools as $key => $tool): ?>

						  	<span class="d-block mb-1"><i class="<?=$tool['icon']?>" data-toggle="tooltip" data-placement="top" title="<?=$tool['name']?> необходим для этой раздачи"></i> <?=$tool['name']?></span>

						  <?php endforeach; endif; ?>

						  
						
						  <!-- <span class="d-block mb-1"><i class="fa-lg fab fa-telegram" data-toggle="tooltip" data-placement="top" title="Телеграм необходим для этой раздачи"></i> Телеграм</span>
						  <span class="d-block mb-1"><i class="fa-lg fas fa-phone-square" data-toggle="tooltip" data-placement="top" title="Номер телефона необходим для этой раздачи"></i> Телефон</span>
						  <span class="d-block mb-1"><i class="fa-lg fab fa-twitter-square" data-toggle="tooltip" data-placement="top" title="Twitter необходим для этой раздачи"></i> Твиттер</span>
						  <span class="d-block mb-1"><i class="fa-lg fas fa-envelope-square" data-toggle="tooltip" data-placement="top" title="E-mail необходим для этой раздачи"></i> E-mail</span>
						  <span class="d-block mb-1"><i class="fa-lg fab fa-facebook-square" data-toggle="tooltip" data-placement="top" title="Facebook необходим для этой раздачи"></i> Facebook</span>
						  <span class="d-block mb-1"><i class="fa-lg fab fa-reddit-square" data-toggle="tooltip" data-placement="top" title="Reddit необходим для этой раздачи"></i> Reddit</span>
						  <span class="d-block mb-1"><i class="fa-lg fab fa-youtube-square" data-toggle="tooltip" data-placement="top" title="Youtube необходим для этой раздачи"></i> Youtube</span>
						  <span class="d-block mb-1"><i class="fa-lg fab fa-linkedin" data-toggle="tooltip" data-placement="top" title="Linkedin необходим для этой раздачи"></i> Linkedin</span>
						  <span class="d-block mb-1"><i class="fa-lg fas fa-address-card" data-toggle="tooltip" data-placement="top" title="KYC необходим для этой раздачи"></i> KYC</span> -->
						
					</div>
				</div>
				<div class="text-center mb-4">
					<h5 class="text-center">Присоединяйтесь к нашим <a href="https://t.me/airolejon" target="_blank">Telegram</a> и <a href="https://www.youtube.com/channel/UCJiCOvnk-Ps65jClBD8MZsA?view_as=subscriber" type="_blank">Youtube</a></h5>
					<a href="<?=$airdrop[0]['start_link']?>" class="btn btn-primary btn-lg" target="_blank">ВЫПОЛНИТЬ AIRDROP</a>
					<div class="row mt-3 align-items-center">
						<div class="col-12 text-center">
								
									<label class="switch">
									  <input type="checkbox" id="switch" name="switch" data-id="<?=$airdrop[0]['id']?>" <?php if($check) echo 'checked'; ?>>

									  <span class="slider round"></span>
									</label>
								
							<span>Отметить как выполненный</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php include 'layout/footer.php'; ?>