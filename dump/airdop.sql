-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 30 2019 г., 11:29
-- Версия сервера: 5.7.21
-- Версия PHP: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `airdop`
--

-- --------------------------------------------------------

--
-- Структура таблицы `airdrops`
--

DROP TABLE IF EXISTS `airdrops`;
CREATE TABLE IF NOT EXISTS `airdrops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `aliace` varchar(255) NOT NULL,
  `num_tokens` int(11) NOT NULL,
  `fiat_price` int(11) NOT NULL,
  `expire_date` int(11) NOT NULL,
  `views` int(11) NOT NULL,
  `descr` text NOT NULL,
  `instruction` text NOT NULL,
  `req_tools` varchar(255) NOT NULL,
  `is_done` int(11) NOT NULL,
  `is_rec` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `airdrops`
--

INSERT INTO `airdrops` (`id`, `name`, `aliace`, `num_tokens`, `fiat_price`, `expire_date`, `views`, `descr`, `instruction`, `req_tools`, `is_done`, `is_rec`) VALUES
(1, 'Жопа', 'first', 5000, 5, 1554864717, 256, 'В сфере недвижимости традиционно доминируют корпорации или группы людей с большими инвестиционными фондами. Мы хотим предоставить возможность каждому участвовать в этом сверхприбыльном бизнесе. Проект UTOPIALAND в отличие от большинства ICO на рынке сейчас, так как мы на самом деле делаем разработки в реальном мире. Первая фаза нашего проекта - омолодить существующий отель с горячими источниками, превратив его в 5-звездочный курорт мирового класса с элитными жилыми виллами, которые можно купить с нашими жетонами. Присоединяйтесь к нам в нашей ICO, чтобы стать частью чего-то реального.', '<li class=\"list-group-item pl-0\"><i class=\"fas fa-arrow-alt-circle-right\"></i> Зайти на страницу airdrop (ВЫПОЛНИТЬ ЭЙРДРОП)</li>\r\n						  <li class=\"list-group-item pl-0\"><i class=\"fas fa-arrow-alt-circle-right\"></i> Отправить свой адрес электронной почты, чтобы присоединиться к waitlist</li>\r\n						  <li class=\"list-group-item pl-0\"><i class=\"fas fa-arrow-alt-circle-right\"></i> Токены будут получены на ваш аккаунт, когда платформа будет запущена</li>\r\n						  <li class=\"list-group-item pl-0\"><i class=\"fas fa-arrow-alt-circle-right\"></i> Porta ac consectetur ac</li>\r\n						  <li class=\"list-group-item pl-0\"><i class=\"fas fa-arrow-alt-circle-right\"></i> Vestibulum at eros</li>', 'Телега', 0, 1),
(2, 'Имя', 'second', 5000, 5, 115656, 256, 'Описание', 'Инструкция', 'Телега', 1, 1),
(3, 'Имя', 'third', 5000, 5, 115656, 256, 'Описание', 'Инструкция', 'Телега', 0, 1),
(4, 'Имя', 'fourth', 5000, 5, 115656, 256, 'Описание', 'Инструкция', 'Телега', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `airdrop_tools`
--

DROP TABLE IF EXISTS `airdrop_tools`;
CREATE TABLE IF NOT EXISTS `airdrop_tools` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `icon` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `airdrop_tools`
--

INSERT INTO `airdrop_tools` (`id`, `name`, `icon`) VALUES
(1, 'Телеграм', 'fa-lg fab fa-telegram'),
(2, 'Телефон', 'fa-lg fas fa-phone-square'),
(3, 'Твиттер', 'fa-lg fab fa-twitter-square'),
(4, 'E-mail', 'fa-lg fas fa-envelope-square'),
(7, 'Facebook', 'fa-lg fab fa-facebook-square'),
(8, 'Reddit', 'fa-lg fab fa-reddit-square'),
(9, 'Youtube', 'fa-lg fab fa-youtube-square'),
(10, 'Linkedin', 'fa-lg fab fa-linkedin'),
(11, 'KYC', 'fa-lg fas fa-address-card');

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `aliace` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`id`, `name`, `aliace`) VALUES
(1, 'Станица', 'page'),
(2, 'Он ас', 'about'),
(3, 'политика конф', 'politics'),
(4, 'Отказ от отв', 'disclaimer'),
(5, 'Правила', 'rules'),
(6, 'Поддержка', 'support');

-- --------------------------------------------------------

--
-- Структура таблицы `tools_airdrops`
--

DROP TABLE IF EXISTS `tools_airdrops`;
CREATE TABLE IF NOT EXISTS `tools_airdrops` (
  `tool_id` int(11) NOT NULL,
  `airdrop_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tools_airdrops`
--

INSERT INTO `tools_airdrops` (`tool_id`, `airdrop_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `name`, `pass`, `role`) VALUES
(1, 'valik', '123', 'admin'),
(2, 'asdasd', 'asdasd', 'user'),
(3, 'admin', '313234234', 'user'),
(4, '121314234', '234232r3', 'user'),
(5, 'Валик не звонитьa', 'asdasdasdas', 'user'),
(6, 'asdasdasd', 'asdasdadasd', 'user'),
(7, 'Карусель игрушки', 'asdasdasd', 'user'),
(8, 'adbfbgdngfhn', 'dgfbgdhngfhn', 'user'),
(9, 'Валик не звонить', 'asdasdasdasd', 'user'),
(10, '2323', 'wesdf23', 'user'),
(11, 'awdadadgesg5eg', 'vegwegegr', 'user'),
(12, 'dfvfbvfgsbf', 'gfbgfbfgb', 'user'),
(13, 'cfdfbfg', 'gfbgfbgfb', 'user'),
(14, 'фысяся', 'ячсячс', 'user'),
(15, 'йцвцв', 'ыясчссвыс', 'user'),
(16, 'ывмаиапи', 'ыаыасывсвысы', 'user'),
(17, 'ыссчсмсмчм', 'чсмчсм', 'user'),
(18, 'фывцувцацмцкац', 'ывмываымывм', 'user'),
(19, 'valikk', 'R3D8397FRin2hWU', 'user');

-- --------------------------------------------------------

--
-- Структура таблицы `user_airdrops`
--

DROP TABLE IF EXISTS `user_airdrops`;
CREATE TABLE IF NOT EXISTS `user_airdrops` (
  `user_id` int(11) NOT NULL,
  `airdrop_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_airdrops`
--

INSERT INTO `user_airdrops` (`user_id`, `airdrop_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
